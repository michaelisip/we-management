@extends('layouts.app')

@section('title')
    Edit Client
@endsection

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
    <div class="flex">
        {{-- Aside --}}
        @include('partials.aside')
        <div class="w-5/6 md:w-4/5 absolute right-0 top-0">
            {{-- Nav --}}
            @include('partials.nav')
            <div class="p-5 md:p-10 z-10 mt-16 bg-admin-gray h-full">
                {{-- Main --}}
                <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                    <div class="px-4 py-5 border-b border-gray-200 sm:px-6">
                        <h3 class="text-xl leading-6 font-medium text-gray-900"> Client Information </h3>
                        <p class="mt-1 max-w-2xl text-sm leading-5 text-gray-500">
                            Use a permanent address where you want can receive mail.
                        </p>
                    </div>
                    <form action="{{ route('clients.update', $client->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method("PUT")
                        <div class="w-full md:w-5/6 px-4 py-5 border-b border-gray-200 sm:px-6">
                            <div class="flex flex-wrap -mx-3 mb-8">
                                <div class="w-full md:w-1/3 px-3 md:mb-0">
                                    <label class="block text-gray-700 font-medium mb-1" for="first_name"> First name <span class="text-red-500 text-md">*</span></label>
                                    <input type="text" name="first_name" id="first_name" value="{{ old('first_name') ?? $client->first_name }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('first_name') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white" autofocus>
                                    @error('first_name')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="w-full md:w-1/3 px-3 md:mb-0">
                                    <label class="block text-gray-700 font-medium mb-1" for="middle_name"> Middle name </label>
                                    <input type="text" name="middle_name" id="middle_name" value="{{ old('middle_name') ?? $client->middle_name }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('middle_name') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white">
                                    @error('middle_name')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="w-full md:w-1/3 px-3">
                                    <label class="block text-gray-700 font-medium mb-1" for="last_name"> Last name </label>
                                    <input type="text" name="last_name" id="last_name" value="{{ old('last_name') ?? $client->last_name }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('last_name') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                    @error('last_name')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="flex flex-wrap -mx-3 mb-8">
                                <div class="w-full md:w-1/4 px-3">
                                    <label class="block text-gray-700 font-medium mb-1" for="suffix"> Suffix </label>
                                    <input type="text" name="suffix" id="suffix" value="{{ old('suffix') ?? $client->suffix }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('suffix') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                    @error('suffix')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="w-full md:w-1/4 px-3">
                                    <label class="block text-gray-700 font-medium mb-1" for="birthday"> Birthday </label>
                                    <input type="date" name="birthday" id="birthday" value="{{ old('birthday') ?? $client->birthday }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('birthday') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                    @error('birthday')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="w-full md:w-2/4 px-3">
                                    <label class="block text-gray-700 font-medium mb-1" for="email"> Email address <span class="text-red-500 text-md">*</span></label>
                                    <input type="email" name="email" id="email" value="{{ old('email') ?? $client->email }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('email') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                    @error('email')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="flex flex-wrap -mx-3 mb-8">
                                <div class="w-full md:w-1/3 px-3 md:mb-0">
                                    <label class="block text-gray-700 font-medium mb-1" for="website_link"> Website </label>
                                    <input type="url" name="website_link" id="website_link" value="{{ old('website_link') ?? $client->website_link }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('website_link') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white" autofocus>
                                    @error('website_link')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="w-full md:w-1/3 px-3 md:mb-0">
                                    <label class="block text-gray-700 font-medium mb-1" for="job_title"> Job Title </label>
                                    <input type="text" name="job_title" id="job_title" value="{{ old('job_title') ?? $client->job_title }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('job_title') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white" autofocus>
                                    @error('job_title')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="w-full md:w-1/3 px-3 md:mb-0">
                                    <label class="block text-gray-700 font-medium mb-1" for="company_name"> Company Name </label>
                                    <input type="text" name="company_name" id="company_name" value="{{ old('company_name') ?? $client->company_name }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('company_name') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white">
                                    @error('company_name')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="w-full md:w-2/3 flex flex-wrap -mx-3 mb-8">
                                <div class="w-full md:w-1/3 px-3">
                                    <label class="block text-gray-700 font-medium mb-1" for="timezone"> Timezone </label>
                                    <input type="text" name="timezone" id="timezone" value="{{ old('timezone') ?? $client->timezone }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('timezone') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                    @error('timezone')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                                {{--
                                    Note: Temporarily assign first address on input
                                --}}
                                @php
                                    $client->address = $client->addresses()->first()
                                @endphp
                                <div class="w-full md:w-2/3 px-3">
                                    <label class="block text-gray-700 font-medium mb-1" for="country"> Country </label>
                                    <div class="relative">
                                        <select name="country" id="country" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('country') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"></select>
                                        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                            <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                        </div>
                                    </div>
                                    @error('country')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="w-full mb-8">
                                <label class="block text-gray-700 font-medium mb-1" for="street"> Street address </label>
                                <input type="text" name="street" id="street" value="{{ $client->address ? $client->address->street : old('street') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('state') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                @error('street')
                                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="flex flex-wrap -mx-3 mb-8">
                                <div class="w-full md:w-1/3 px-3 md:mb-0">
                                    <label class="block text-gray-700 font-medium mb-1" for="city"> City </label>
                                    <input type="text" name="city" id="city" value="{{ $client->address ? $client->address->city : old('city') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('city') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white">
                                    @error('city')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="w-full md:w-1/3 px-3">
                                    <label class="block text-gray-700 font-medium mb-1" for="state"> State / Province </label>
                                    <input type="text" name="state" id="state" value="{{ $client->address ? $client->address->state : old('state') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('state') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                    @error('state')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="w-full md:w-1/3 px-3">
                                    <label class="block text-gray-700 font-medium mb-1" for="zip"> Zip / Postal </label>
                                    <input type="text" name="zip" id="zip" value="{{ $client->address ? $client->address->zip : old('zip') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('zip') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                    @error('zip')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="w-full mb-8">
                                <label class="block text-gray-700 font-medium mb-1" for="background_info"> Background Information </label>
                                <textarea name="background_info" id="background_info" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('background_info') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" cols="30" rows="4">{{ old('background_info') ?? $client->background_info }}</textarea>
                                <p class="text-gray-600 text-sm mt-2">Brief description for client. URLs are hyperlinked.</p>
                                @error('background_info')
                                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="w-full md:w-4/5 flex flex-wrap -mx-3 mb-8">
                                <div class="w-full md:w-1/3 px-3" id="type-input">
                                    <label class="block text-gray-700 font-medium mb-1" for="type"> Type </label>
                                    <select name="type" id="type" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('type') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                        <option value="public" @if (old('type') === 'public' || $client->type === "public") selected @endif> Public </option>
                                        <option value="broker" @if (old('type') === "broker" || $client->type === 'broker') selected @endif> Broker </option>
                                    </select>
                                    @error('type')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="w-full md:w-1/3 px-3" id="status-input">
                                    <label class="block text-gray-700 font-medium mb-1" for="status"> Status </label>
                                    <select name="status" id="status" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('status') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                        <option value="active" @if (old('status') === 'active' || $client->status === "active") selected @endif> Active </option>
                                        <option value="inactive" @if (old('status') === "inactive" || $client->status === 'inactive') selected @endif> Inactive </option>
                                        <option value="archived" @if (old('status') === "archived" || $client->status === 'archived') selected @endif> Archived </option>
                                    </select>
                                    @error('status')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="w-full md:w-1/3 px-3 hidden" id="archived-at-input">
                                    <label class="block text-gray-700 font-medium mb-1" for="archived_at"> Archived At </label>
                                    <input type="datetime-local" name="archived_at" id="archived_at" value="@if (old('archived_at')){{ old('archived_at') }}@elseif ($client->archived_at){{ $client->archived_at->format('Y-m-d\TH:i') }}@endif" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('archived_at') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                    @error('archived_at')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="w-full md:w-2/3 mb-8">
                                <label class="block text-gray-700 font-medium mb-1" for="image"> Photo </label>
                                <input type="file" name="image" id="image" class="appearance-none block w-full text-gray-700 border border-gray-400 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                            </div>
                            <div class="w-full md:w-2/3 mb-8">
                                <label class="block text-gray-700 font-medium mb-1" for="file"> Files </label>
                                <input type="file"name="file[]" id="file"  class="appearance-none block w-full text-gray-700 border border-gray-400 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" multiple>
                            </div>
                            <div class="w-full md:w-2/3 mb-8">
                                <label class="block text-gray-700 font-medium mb-1" for="group"> Groups </label>
                                <select name="group[]" id="group" class="appearance-none block w-full text-gray-700 border border-gray-400 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" multiple>
                                    @foreach ($client->groups as $group)
                                        <option value="{{ $group->id }}" selected> {{ $group->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="flex justify-end bg-gray-100 px-8 py-3">
                            <button type="submit" class="bg-indigo-600 hover:bg-indigo-700 text-white py-2 px-8 rounded-md"> Save </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            showArchivedAt()
            $('#group').select2({
                ajax: {
                    url: "{{ route('selections.groups') }}",
                    dataType: 'json',
                    processResults: function (data) {
                        return {
                            results: data.results
                        }
                    },
                    delay: 250,
                    cache: true,
                },
                minimumInputLength: 1,
                tags: true,
                tokenSeparators: [',']
            });
            $.get('https://restcountries.eu/rest/v2/all', function(data) {
                $.each(data, function(index, { name }) {
                    var isSelected = name === "{{ $client->address->country }}"
                    $('#country').append(new Option(name, name, isSelected, isSelected))
                })
            })
        });

        $('#status').on('change', function() {
            showArchivedAt()
        })

        function showArchivedAt() {
            if ($('#status').val() === 'archived') {
                $('#archived-at-input').removeClass('hidden')
            } else {
                $('#archived-at-input').addClass('hidden')
            }
        }

    </script>
@endpush

