@extends('layouts.app')

@section('title')
    Clients
@endsection

@section('content')
    <div class="flex">

        {{-- Aside --}}
        @include('partials.aside')

        {{-- Content --}}
        <div class="w-5/6 md:w-4/5 absolute right-0 top-0">
            @include('partials.nav')
            <div class="bg-admin-gray @if ($clients->count()) h-full @else h-screen @endif p-5 md:pt-24 md:pb-10 md:pl-10 md:pr-10 z-10">
                <div class="flex items-center justify-between mb-5">
                    <div>
                        <form method="GET" class="inline" id="clients-search">
                            <input class="bg-white appearance-none border border-gray-300 rounded py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white" type="text" id="search-clients" name="search" value="{{ request('search') }}" placeholder="Search clients">
                        </form>
                        <div class="relative inline-block text-left">
                            <div>
                                <span class="rounded-md shadow-sm">
                                    <button type="button" class="inline-flex justify-center w-full rounded border border-gray-300 px-4 py-2 bg-gray-100 text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition ease-in-out duration-150" id="sort" aria-haspopup="true" aria-expanded="true">
                                        Sort
                                        <svg class="-mr-1 ml-2 h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                            <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                        </svg>
                                    </button>
                                </span>
                            </div>
                            <div class="origin-top-right absolute left-0 mt-2 w-56 rounded-md shadow-lg hidden" id="sort-dropdown">
                                <div class="rounded-md bg-white shadow-xs">
                                    <div class="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                                        <a href="{{ route('clients.index', ['sort' => 'first_name', 'search' => request('search'), 'desc' => request('desc')]) }}" class="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900" role="menuitem">
                                            Name
                                        </a>
                                        <a href="{{ route('clients.index', ['sort' => 'job_title', 'search' => request('search'), 'desc' => request('desc')]) }}" class="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900" role="menuitem">
                                            Title
                                        </a>
                                        <a href="{{ route('clients.index', ['sort' => 'status', 'search' => request('search'), 'desc' => request('desc')]) }}" class="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900" role="menuitem">
                                            Status
                                        </a>
                                        <a href="{{ route('clients.index', ['sort' => 'type', 'search' => request('search'), 'desc' => request('desc')]) }}" class="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900" role="menuitem">
                                            Type
                                        </a>
                                        <a href="{{ route('clients.index', ['sort' => 'created_at', 'search' => request('search'), 'desc' => request('desc')]) }}" class="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900" role="menuitem">
                                            Creation Date
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <a href="{{ route('clients.create') }}" class="px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:shadow-outline-indigo focus:border-indigo-700 active:bg-indigo-700 transition duration-150 ease-in-out">Create New</a>
                    </div>
                </div>
                <div class="flex items-center justify-between mb-2">
                    <h1 class="text-2xl font-medium"> Client List </h1>
                </div>
                <div class="flex flex-col">
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                <table class="min-w-full divide-y divide-gray-200">
                                    <thead>
                                        <tr>
                                            <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                                Name
                                            </th>
                                            <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                                Title
                                            </th>
                                            <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                            </th>
                                            <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                                Type
                                            </th>
                                            <th class="px-6 py-3 bg-gray-50"></th>
                                        </tr>
                                    </thead>
                                    <tbody class="bg-white divide-y divide-gray-200">
                                        @forelse ($clients as $client)
                                            <tr>
                                                <td class="px-6 py-4 whitespace-no-wrap">
                                                    <div class="flex items-center">
                                                        <div class="flex-shrink-0 h-10 w-10">
                                                            <img class="h-10 w-10 rounded-full" src="{{ $client->image->url ?? asset('images/placeholder.jpg') }}" alt="{{ $client->name }}">
                                                        </div>
                                                        <div class="ml-4">
                                                            <div class="text-sm leading-5 font-medium text-gray-900">
                                                                {{ ucwords($client->fullname) }}
                                                            </div>
                                                            <div class="text-sm leading-5 text-gray-500">
                                                                {{ $client->email }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="px-6 py-4 whitespace-no-wrap">
                                                    <div class="text-sm leading-5 text-gray-900">
                                                        @if ($client->job_title)
                                                            {{ ucwords($client->job_title) }}
                                                        @else
                                                            <i> No job indicated </i>
                                                        @endif
                                                    </div>
                                                    <div class="text-sm leading-5 text-gray-500">
                                                        @if ($client->company_name)
                                                            {{ ucwords($client->company_name) }}
                                                        @else
                                                            <i> No company indicated </i>
                                                        @endif
                                                    </div>
                                                </td>
                                                <td class="px-6 py-4 whitespace-no-wrap">
                                                    <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full {{ $client->style_classes }}">
                                                        {{ ucwords($client->status) }}
                                                    </span>
                                                </td>
                                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                                    {{ ucwords($client->type) }}
                                                </td>
                                                <td class="px-6 py-4 whitespace-no-wrap text-right text-sm leading-5 font-medium">
                                                    <a href="{{ route('clients.show', $client->id) }}" class="text-indigo-600 hover:text-indigo-900">View</a>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td class="px-6 py-4 whitespace-no-wrap" colspan="5">
                                                    <i> No entries yet </i>
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                                @if ($clients->count())
                                    <div class="m-2 px-3">
                                        {{ $clients->withQueryString()->links('pagination::tailwind') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#sort').on('click', function() {
            $('#sort-dropdown').toggle('hidden')
        })
    </script>
@endpush
