@extends('layouts.app')

@section('title')
    Client Information
@endsection

@section('content')
    <div class="flex">
        {{-- Aside --}}
        @include('partials.aside')
        <div class="w-5/6 md:w-4/5 absolute right-0 top-0">
            {{-- Nav --}}
            @include('partials.nav')
            <div class="p-5 md:p-10 z-10 mt-16 bg-admin-gray h-full">
                {{-- Main --}}
                <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                    <div class="flex justify-between items-center border-b border-gray-200 p-8">
                        <div>
                            <h3 class="text-xl leading-6 font-medium text-gray-900"> Client Information </h3>
                            <p class="mt-1 max-w-2xl text-sm leading-5 text-gray-500">
                                Personal details and application
                            </p>
                        </div>
                        <div>
                            <a href="{{ route('clients.edit', $client->id) }}" class="rounded-lg border border-gray-400 inline-flex items-center py-2 px-5 mr-3">
                                <svg class="w-6 h-6 mr-1" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"></path></svg>
                                <span class="text-lg">Edit</span>
                            </a>
                            {{-- <a href="{{ route('clients.edit', $client->id) }}" class="rounded-lg border border-gray-400 inline-flex items-center py-2 px-5 mr-3">
                                <svg class="w-6 h-6 mr-1" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"></path></svg>
                                <span class="text-lg">Emails</span>
                            </a> --}}
                        </div>
                    </div>
                    <div class="border-b border-gray-200 p-8">
                        <div class="flex flex-wrap -mx-3 mb-8">
                            <div class="w-full md:w-1/2 px-3 md:mb-0">
                                <label class="block text-gray-600 mb-1"> Name </label>
                                <span> {{ ucwords($client->fullname) }} </span>
                            </div>
                            <div class="w-full md:w-1/2 px-3 md:mb-0">
                                <label class="block text-gray-600 mb-1"> Client Type </label>
                                <span> {{ ucwords($client->type) }} </span>
                            </div>
                        </div>
                        <div class="flex flex-wrap -mx-3 mb-8">
                            <div class="w-full md:w-1/2 px-3 md:mb-0">
                                <label class="block text-gray-600 mb-1"> Email address </label>
                                <span> {{ $client->email }} </span>
                            </div>
                            <div class="w-full md:w-1/2 px-3 md:mb-0">
                                <label class="block text-gray-600 mb-1"> Status </label>
                                <span> {{ ucwords($client->status) }} Client </span>
                            </div>
                        </div>
                        <div class="flex flex-wrap -mx-3 mb-8">
                            <div class="w-full md:w-1/2 px-3 md:mb-0">
                                <label class="block text-gray-600 mb-1"> Job Title </label>
                                @if ($client->job_title)
                                    <span> {{ $client->job_title }} </span>
                                @else
                                    <i> Not indicated </i>
                                @endif
                            </div>
                            <div class="w-full md:w-1/2 px-3 md:mb-0">
                                <label class="block text-gray-600 mb-1"> Company Name </label>
                                @if ($client->company_name)
                                    <span> {{ ucwords($client->company_name) }}</span>
                                @else
                                    <i> Not indicated </i>
                                @endif
                            </div>
                        </div>
                        <div class="mb-8">
                            <label class="block text-gray-600 mb-1"> Background Information </label>
                            <span> {{ $client->background_info }} </span>
                        </div>
                        <div class="mb-8">
                            <label class="block text-gray-600 mb-1"> Files </label>
                            <div class="flex flex-col">
                                @forelse ($client->files as $file)
                                    <div class="w-full flex justify-between items-center border border-gray-400 @if ($loop->first) rounded-t-lg border-b-0 @endif @if ($loop->last) rounded-b-lg border-t-0 @endif px-5 py-3">
                                        <div class="flex flex-row items-center">
                                            <svg class="w-8 h-8 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 10v6m0 0l-3-3m3 3l3-3m2 8H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"></path></svg>
                                            {{ $file->filename }}
                                        </div>
                                        <a href="{{ $file->url }}" class="text-indigo-700 font-medium" download>Download</a>
                                    </div>
                                @empty
                                    <i class="text-sm"> Client does not have any file/s yet. </i>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
