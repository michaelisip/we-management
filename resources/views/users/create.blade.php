@extends('layouts.app')

@section('title')
    Create User
@endsection

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
    <div class="flex">
        {{-- Aside --}}
        @include('partials.aside')
        <div class="w-5/6 md:w-4/5 absolute right-0 top-0">
            {{-- Nav --}}
            @include('partials.nav')
            <div class="p-5 md:p-10 z-10 mt-16 bg-admin-gray h-full">
                {{-- Main --}}
                <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                    <div class="px-4 py-5 border-b border-gray-200 sm:px-6">
                        <h3 class="text-xl leading-6 font-medium text-gray-900"> User Information </h3>
                        <p class="mt-1 max-w-2xl text-sm leading-5 text-gray-500">
                            Use a permanent address where you want can receive mail.
                        </p>
                    </div>
                    <form action="{{ route('users.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="w-full md:w-5/6 px-4 py-5 border-b border-gray-200 sm:px-6">
                            <div class="w-full md:w-2/3 mb-8">
                                <label class="block text-gray-700 font-medium mb-1" for="image"> Photo </label>
                                <input type="file" name="image" id="image" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('image') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                @error('image')
                                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="flex flex-wrap -mx-3 mb-8">
                                <div class="w-full md:w-1/3 px-3 md:mb-0">
                                    <label class="block text-gray-700 font-medium mb-1" for="first_name"> First name <span class="text-red-500 text-md">*</span></label>
                                    <input type="text" name="first_name" id="first_name" value="{{ old('first_name') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('first_name') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white" autofocus>
                                    @error('first_name')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="w-full md:w-1/3 px-3">
                                    <label class="block text-gray-700 font-medium mb-1" for="last_name"> Last name </label>
                                    <input type="text" name="last_name" id="last_name" value="{{ old('last_name') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('last_name') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                    @error('last_name')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="w-full md:w-2/3 mb-8">
                                <label class="block text-gray-700 font-medium mb-1" for="roles"> Roles </label>
                                <select name="roles[]" id="roles" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('roles') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" multiple></select>
                                @error('roles')
                                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="w-full md:w-2/3 mb-8">
                                <label class="block text-gray-700 font-medium mb-1" for="email"> Email address <span class="text-red-500 text-md">*</span></label>
                                <input type="email" name="email" id="email" value="{{ old('email') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('email') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                @error('email')
                                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="w-full md:w-2/3 mb-8">
                                <label class="block text-gray-700 font-medium mb-1" for="password"> Password <span class="text-red-500 text-md">*</span></label>
                                <input type="password" name="password" id="password" value="{{ old('password') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('password') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                @error('password')
                                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="w-full md:w-2/3 mb-8">
                                <label class="block text-gray-700 font-medium mb-1" for="password_confirmation"> Confirm Password <span class="text-red-500 text-md">*</span></label>
                                <input type="password" name="password_confirmation" id="password_confirmation" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('password_confirmation') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                            </div>
                        </div>
                        <div class="flex justify-end bg-gray-100 px-8 py-3">
                            <button type="submit" class="bg-indigo-600 hover:bg-indigo-700 text-white py-2 px-8 rounded-md"> Save </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#roles').select2({
                ajax: {
                    url: "{{ route('selections.roles') }}",
                    dataType: 'json',
                    processResults: function (data) {
                        return {
                            results: data.results
                        }
                    },
                    delay: 250,
                    cache: true,
                },
                minimumInputLength: 1,
            });
        });
    </script>
@endpush

