@extends('layouts.app')

@section('title')
    Discovery Form Sample
@endsection

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
    <div class="flex">
        {{-- Aside --}}
        @include('partials.aside')
        <div class="w-5/6 md:w-4/5 absolute right-0 top-0">
            {{-- Nav --}}
            @include('partials.nav')
            <div class="bg-admin-gray h-screen p-5 md:pt-24 md:pb-10 md:pl-10 md:pr-10 z-10">
                {{-- Main --}}
                <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                    <div class="px-4 py-5 border-b border-gray-200 sm:px-6">
                        <h3 class="text-xl leading-6 font-medium text-gray-900"> Discovery Form Sample </h3>
                        <p class="mt-1 max-w-2xl text-sm leading-5 text-red-500 italic">
                            Please note that this is just a sample form for the triggering the discovery form api.
                        </p>
                    </div>
                    <form action="{{ route('discovery.trigger') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="type" value="public">
                        <div class="w-full md:w-5/6 px-4 py-5 border-b border-gray-200 sm:px-6">
                            <div class="flex flex-wrap -mx-3 mb-8">
                                <div class="w-full md:w-1/3 px-3 md:mb-0">
                                    <label class="block text-gray-700 font-medium mb-1" for="first_name"> First name <span class="text-red-500 text-md">*</span></label>
                                    <input type="text" name="first_name" id="first_name" value="{{ old('first_name') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('first_name') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white" autofocus>
                                    @error('first_name')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="w-full md:w-1/3 px-3">
                                    <label class="block text-gray-700 font-medium mb-1" for="last_name"> Last name </label>
                                    <input type="text" name="last_name" id="last_name" value="{{ old('last_name') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('last_name') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                    @error('last_name')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="w-full md:w-2/4 mb-8">
                                <label class="block text-gray-700 font-medium mb-1" for="email"> Email address <span class="text-red-500 text-md">*</span></label>
                                <input type="email" name="email" id="email" value="{{ old('email') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('email') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                @error('email')
                                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="flex flex-col mb-5">
                                <div class="md:w-1/3"></div>
                                <label class="md:w-2/3 block">
                                    <input class="mr-2 leading-tight" type="checkbox" name="appointment" id="appointment" @if(old('appointment') !== null) checked @endif>
                                    <label class="text-gray-700 font-medium text-lg" for="appointment">
                                        Schedule an appointment
                                    </label>
                                </label>
                            </div>
                            <div class="flex flex-wrap -mx-3 mb-8">
                                <div class="w-full md:w-1/3 px-3" id="appointment_type_field">
                                    <label class="block text-gray-700 font-medium mb-1" for="type"> Appointment Type <span class="text-red-500 text-md">*</span></label>
                                    <div class="relative">
                                        <select name="appointment_type" id="appointment_type" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('appointment_type') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                            <option value="consultation-call" @if (old('appointment_type') === null || old('appointment_type') === "consultation-call") selected @endif> Consultation Call </option>
                                            <option value="others" @if (old('appointment_type') === "others") selected @endif> Others </option>
                                        </select>
                                        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                            <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                        </div>
                                    </div>
                                    @error('appointment_type')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="w-full md:w-1/3 px-3" id="appointment_date_time_field">
                                    <label class="block text-gray-700 font-medium mb-1" for="appointment_date_time"> Date / Time <span class="text-red-500 text-md">*</span></label>
                                    <input type="datetime-local" name="appointment_date_time" id="appointment_date_time" value="{{ old('appointment_date_time') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('appointment_date_time') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                    @error('appointment_date_time')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="flex justify-end bg-gray-100 px-8 py-3">
                            <button type="submit" class="bg-indigo-600 hover:bg-indigo-700 text-white py-2 px-8 rounded-md"> Submit </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            addOrRemoveAppointmentFields()
        })
        $('#appointment').on('change', function () {
            addOrRemoveAppointmentFields()
        })

        function addOrRemoveAppointmentFields() {
            if ($('#appointment').is(':checked')) {
                $('#appointment_type_field').removeClass('hidden')
                $('#appointment_date_time_field').removeClass('hidden')
            } else {
                $('#appointment_type_field').addClass('hidden')
                $('#appointment_date_time_field').addClass('hidden')
            }
        }
    </script>
@endpush
