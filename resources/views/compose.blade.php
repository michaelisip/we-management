@extends('layouts.app')

@section('title')
    Compose Email
@endsection

@push('head')
    <script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
@endpush

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
    <div class="flex">
        @include('partials.aside')
        <div class="w-5/6 md:w-4/5 absolute right-0 top-0">
            @include('partials.nav')
            <div class="p-5 md:p-10 z-10 mt-16 bg-admin-gray h-full">
                <div class="flex justify-between">
                    <h1 class="text-2xl font-bold">Compose Email</h1>
                </div>
                @include('partials.messages')
                <div class="bg-white shadow sm:rounded-lg mt-5 p-5">
                    <form action="{{ route('mail.send') }}" method="post">
                        @csrf
                        <div class="flex flex-col mb-5">
                            <label for="template" class="font-bold text-gray-700 mb-1">Template </label>
                            <select class="w-full md:w-1/3" id="template"></select>
                        </div>
                        <div class="flex flex-col mb-5">
                            <label for="to" class="font-bold text-gray-700 mb-1">To <span class="text-red-500 text-xs">*</span></label>
                            <select class="w-full md:w-1/3" name="to" id="to"></select>
                        </div>
                        <div class="flex flex-col mb-5">
                            <label for="cc" class="font-bold text-gray-700 mb-1">Cc</label>
                            <select class="w-full md:w-1/3" name="cc[]" id="cc" multiple></select>
                        </div>
                        <div class="flex flex-col mb-5">
                            <label for="bcc" class="font-bold text-gray-700 mb-1">Bcc</label>
                            <select class="w-full md:w-1/3" name="bcc[]" id="bcc" multiple></select>
                        </div>
                        <div class="flex flex-col mb-5">
                            <label for="subject" class="font-bold text-gray-700 mb-1">Subject <span class="text-red-500 text-xs">*</span></label>
                            <input type="text" name="subject" id="subject" class="border-2 rounded w-full md:w-1/3" value="{{ old('subject') }}">
                        </div>
                        <div class="flex flex-col mb-5">
                            <label for="body" class="font-bold text-gray-700 mb-1">Body <span class="text-red-500 text-xs">*</span></label>
                            <textarea name="body" id="body" class="border-2 rounded w-full md:w-1/3" cols="30" rows="10">{{ old('body') }}</textarea>
                        </div>
                        <div class="flex flex-col mb-5">
                            <label for="attachments" class="font-bold text-gray-700 mb-1">Attachments</label>
                            <label class="w-64 flex flex-col items-center px-2 py-6 bg-white text-blue rounded-lg shadow-lg tracking-wide border border-blue cursor-pointer hover:bg-blue hover:text-white">
                                <svg class="w-8 h-8" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path d="M16.88 9.1A4 4 0 0 1 16 17H5a5 5 0 0 1-1-9.9V7a3 3 0 0 1 4.52-2.59A4.98 4.98 0 0 1 17 8c0 .38-.04.74-.12 1.1zM11 11h3l-4-4-4 4h3v3h2v-3z" />
                                </svg>
                                <input type="file" name="attachments[]" id="attachments" class="hidden" multiple/>
                            </label>
                        </div>
                        <div class="flex justify-end mt-10">
                            <button class="px-5 py-2 bg-admin-blue-dark text-white font-bold rounded-lg" type="submit">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="{{ asset('js/insert_variable.js') }}"></script>
    <script>
        CKEDITOR.replace('body', {
            filebrowserImageUploadUrl: "{{ route('templates.ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
            extraPlugins: 'insert_variable',
            insert_variable: {
                variables: [
                    'To: First Name',
                    'To: Email',
                    'From: First Name',
                    'From: Email',
                ]
            },
        });

        $('select').select2({
            tags: true,
            tokenSeparators: [',', ' ']
        })
        $('#template').select2({
            ajax: {
                url: "{{ route('selections.templates') }}",
                dataType: 'json',
                processResults: function (data) {
                    return {
                        results: data.results
                    }
                },
                delay: 250,
                cache: true,
            },
            minimumInputLength: 1,
            tags: true,
            tokenSeparators: [',', ' ']
        });

        $('#to').select2({
            ajax: {
                url: "{{ route('selections.clients') }}",
                dataType: 'json',
                processResults: function (data) {
                    return {
                        results: data.results
                    }
                },
                delay: 250,
                cache: true,
            },
            templateResult: function (state) {
                if (! state.id) {
                    return state.text
                }
                return `${state.fullname} - ${state.text}`
            },
            templateSelection: function (state) {
                if (! state.id) {
                    return state.text
                }
                return `${state.text}`
            },
            minimumInputLength: 2,
        });

        $('#template').change(function () {
            var route = '{{ route("templates.fill", ":id") }}'
            route = route.replace(':id', this.value)

            $.get(route, function ({ template }) {
                fill(template)
            })
        })

        function fill({ to, bcc, cc, subject, body }) {
            // remove first
            $('#to option').remove()
            $('#bcc option').remove()
            $('#cc option').remove()

            // yolo
            if (to) to.forEach(item => { $('#to').append(`<option value=" ${item}" selected="selected"> ${item} </option>`)});
            if (bcc) bcc.forEach(item => { $('#bcc').append(`<option value=" ${item}" selected="selected"> ${item} </option>`)});
            if (cc) cc.forEach(item => { $('#cc').append(`<option value=" ${item}" selected="selected"> ${item} </option>`)});

            $('#subject').val(subject)
            CKEDITOR.instances['body'].setData(body)
        }

    </script>
@endpush
