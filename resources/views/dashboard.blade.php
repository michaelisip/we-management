@extends('layouts.app')

@section('title')
    Dashboard
@endsection

@section('content')
    <div class="flex">

        {{-- Aside --}}
        @include('partials.aside')

        {{-- Content --}}
        <div class="w-5/6 md:w-4/5 absolute right-0 top-0">
            @include('partials.nav')
            <div class="p-5 md:p-10 z-10 mt-16 bg-admin-gray h-full">
                <div class="mb-8">
                    <h1 class="text-2xl text-gray-800 font-medium">Statistics</h1>
                    <div class="flex flex-row mt-3 -mx-3">
                        <div class="w-full md:w-1/3 bg-white shadow overflow-hidden rounded-lg mx-3 px-4 py-2">
                            <p class="text-md text-gray-700 pt-4 pl-4">Active Clients</p>
                            <h1 class="text-4xl text-gray-900 font-medium pl-4 -mt-2 mb-2">{{ number_format($activeClients) }}</h1>
                        </div>
                        <div class="w-full md:w-1/3 bg-white shadow overflow-hidden rounded-lg mx-3 px-4 py-2">
                            <p class="text-md text-gray-700 pt-4 pl-4">Leads</p>
                            <h1 class="text-4xl text-gray-900 font-medium pl-4 -mt-2 mb-2">579</h1>
                        </div>
                        <div class="w-full md:w-1/3 bg-white shadow overflow-hidden rounded-lg mx-3 px-4 py-2">
                            <p class="text-md text-gray-700 pt-4 pl-4">Pending</p>
                            <h1 class="text-4xl text-gray-900 font-medium pl-4 -mt-2 mb-2">{{ number_format($pendingAppointments) }}</h1>
                        </div>
                    </div>
                </div>
                <div class="mb-8">
                    <h1 class="text-2xl text-gray-800 font-medium">Recent Activity</h1>
                    <div class="bg-white shadow overflow-hidden sm:rounded-lg mt-3">
                        @if ($recentActivities->count())
                            <div class="flex flex-col mt-5">
                                <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
                                    <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                                        <table class="min-w-full">
                                            <thead>
                                                <tr>
                                                    <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider" >
                                                        Name
                                                    </th>
                                                    <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider" >
                                                        Role
                                                    </th>
                                                    <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider" >
                                                        Email
                                                    </th>
                                                    <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider" >
                                                        Creation Date
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody class="bg-white">
                                                @foreach ($recentActivities as $activity)
                                                    <tr>
                                                        <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-900">
                                                            {{ $activity->fullname }}
                                                        </td>
                                                        <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-900">
                                                            {{ $role->title }}
                                                        </td>
                                                        <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-900">
                                                            {{ $activity->email }}
                                                        </td>
                                                        <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-900">
                                                            {{ $activity->created_at->diffForHumans() }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="bg-white shadow sm:rounded-lg p-5">
                                <h6>No entries yet.</h6>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
