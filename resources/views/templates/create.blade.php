@extends('layouts.app')

@section('title')
    Create Template
@endsection

@push('head')
    <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
@endpush

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
    <div class="flex">
        {{-- Aside --}}
        @include('partials.aside')
        <div class="w-5/6 md:w-4/5 absolute right-0 top-0">
            {{-- Nav --}}
            @include('partials.nav')
            <div class="p-5 md:p-10 z-10 mt-16 bg-admin-gray h-full">
                {{-- Main --}}
                <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                    <div class="px-4 py-5 border-b border-gray-200 sm:px-6">
                        <h3 class="text-xl leading-6 font-medium text-gray-900"> Create Template </h3>
                        <p class="mt-1 max-w-2xl text-sm leading-5 text-gray-500">
                            Create reusable email template.
                        </p>
                    </div>
                    <form action="{{ route('templates.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="w-full md:w-5/6 px-4 py-5 border-b border-gray-200 sm:px-6">
                            <div class="flex flex-wrap -mx-3 mb-8">
                                <div class="w-full md:w-1/2 px-3 md:mb-0">
                                    <label class="block text-gray-700 font-medium mb-1" for="name"> Template name <span class="text-red-500 text-md">*</span></label>
                                    <input type="text" name="name" id="name" value="{{ old('name') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('name') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white" autofocus>
                                    @error('name')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="w-full md:w-1/2 px-3 md:mb-0">
                                    <label class="block text-gray-700 font-medium mb-1" for="keyword"> Keyword </label>
                                    <input type="text" name="keyword" id="keyword" value="{{ old('keyword') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('keyword') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white">
                                    @error('keyword')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="flex flex-col mb-8">
                                <label class="block text-gray-700 font-medium mb-1" for="body"> Template content <span class="text-red-500 text-md">*</span></label>
                                <textarea name="body" id="body" class="border-2 rounded w-full" cols="30" rows="10">{{ old('body') }}</textarea>
                            </div>
                            <label class="block text-gray-700 font-medium mb-1" for="attachments"> Add attachments </label>
                            <div class="border border-dashed border-gray-500 relative mb-5">
                                <input type="file" name="attachments[]" id="attachments" value="{{ old('attachments') }}" multiple class="cursor-pointer relative block opacity-0 w-full h-full p-20 z-50">
                                <div class="text-center p-10 absolute top-0 right-0 left-0 m-auto">
                                    <h4>
                                        <span class="text-indigo-500">Upload a file</span> or drag and drop
                                        <br> PNG, JPG, up to 10mb 
                                    </h4>
                                </div>
                            </div>
                            <div class="flex flex-col mb-8 hidden" id="attachmentsDiv">
                                <label class="block text-gray-700 font-medium mb-1"> Attachments </label>
                                <div class="flex flex-col mb-1" id="attachmentBody"></div>
                            </div>
                            <div class="w-full mb-8">
                                <label class="block text-gray-700 font-medium mb-1" for="subject"> Subject </label>
                                <input type="text" name="subject" id="subject" value="{{ old('subject') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('subject') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                @error('subject')
                                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="flex flex-wrap -mx-3 mb-8">
                                <div class="w-full md:w-1/3 px-3 md:mb-0">
                                    <label class="block text-gray-700 font-medium mb-1" for="to"> To </label>
                                    <select class="w-full" name="to[]" id="to" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('to') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" multiple>
                                        @foreach ((array) old('to') as $to)
                                            <option value="{{ $to }}" selected> {{ $to }} </option>
                                        @endforeach
                                    </select>
                                    @error('to')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="w-full md:w-1/3 px-3 md:mb-0">
                                    <label class="block text-gray-700 font-medium mb-1" for="cc"> Cc </label>
                                    <select class="w-full" name="cc[]" id="cc" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('cc') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" multiple>
                                        @foreach ((array) old('cc') as $cc)
                                            <option value="{{ $cc }}" selected> {{ $cc }} </option>
                                        @endforeach
                                    </select>
                                    @error('cc')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="w-full md:w-1/3 px-3 md:mb-0">
                                    <label class="block text-gray-700 font-medium mb-1" for="bcc"> Bcc </label>
                                    <select class="w-full" name="bcc[]" id="bcc" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('bcc') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" multiple>
                                        @foreach ((array) old('bcc') as $bcc)
                                            <option value="{{ $bcc }}" selected> {{ $bcc }} </option>
                                        @endforeach
                                    </select>
                                    @error('bcc')
                                        <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="w-full md:w-1/2 mb-8">
                                <label class="block text-gray-700 font-medium mb-1" for="tags"> Tags </label>
                                <select name="tags[]" id="tags" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('tags') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" multiple></select>
                                @error('tags')
                                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="flex justify-end bg-gray-100 px-8 py-3">
                            <button type="submit" class="bg-indigo-600 hover:bg-indigo-700 text-white py-2 px-8 rounded-md"> Save </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('js/insert_variable.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('select').select2({
                tags: true,
                tokenSeparators: [',', ' ']
            })
            $('#to').select2({
                tags: true,
                tokenSeparators: [',', ' '],
                maximumSelectionLength: 1
            })
            $('#tags').select2({
                ajax: {
                    url: "{{ route('selections.tags') }}",
                    dataType: 'json',
                    processResults: function (data) {
                        return {
                            results: data.results
                        }
                    },
                    delay: 250,
                    cache: true,
                },
                minimumInputLength: 1,
                tags: true,
                tokenSeparators: [',']
            });

            $('input#attachments').on('change', function(){
                let attachments = Array.from(this.files).map(function(f) {
                    return f.name;
                });

                $.each(attachments, function(index, el) {
                    console.log($('#attachments').val())
                    let attachment = `
                                    <div class="w-full flex justify-between items-center border border-gray-400 ${attachments.length } px-5 py-3">
                                        <div class="flex flex-row items-center">
                                            <svg class="w-8 h-8 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 10v6m0 0l-3-3m3 3l3-3m2 8H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"></path></svg>
                                            ${el}
                                        </div>
                                    </div>`;

                    $('div#attachmentBody').append(attachment)
                    $('div#attachmentsDiv').show()
                });
            })
        });

        CKEDITOR.replace('body', {
            filebrowserImageUploadUrl: "{{ route('templates.ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
            extraPlugins: 'insert_variable',
            insert_variable: {
                variables: [
                    'To: First Name',
                    'To: Email',
                    'From: First Name',
                    'From: Email',
                ]
            },
        });
    </script>
@endpush
