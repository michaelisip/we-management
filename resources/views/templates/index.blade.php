@extends('layouts.app')

@section('title')
    Email Templates
@endsection

@section('content')
    <div class="flex">

        {{-- Aside --}}
        @include('partials.aside')

        {{-- Content --}}
        <div class="w-5/6 md:w-4/5 absolute right-0 top-0">
            @include('partials.nav')
            <div class="bg-admin-gray @if ($templates->count() > 5) h-full @else h-screen @endif p-5 md:pt-24 md:pb-10 md:pl-10 md:pr-10 z-10">
                <div class="flex items-center justify-between mb-5">
                    <div>
                        <form method="GET" class="inline" id="templates-search">
                            <input class="bg-white appearance-none border border-gray-300 rounded py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white" type="text" id="search-templates" name="search" value="{{ request('search') }}" placeholder="Search templates">
                        </form>
                        <div class="relative inline-block text-left">
                            <div>
                                <span class="rounded-md shadow-sm">
                                    <button type="button" class="inline-flex justify-center w-full rounded border border-gray-300 px-4 py-2 bg-gray-100 text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition ease-in-out duration-150" id="sort" aria-haspopup="true" aria-expanded="true">
                                        Sort
                                        <svg class="-mr-1 ml-2 h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                            <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                        </svg>
                                    </button>
                                </span>
                            </div>
                            <div class="origin-top-right absolute left-0 mt-2 w-56 rounded-md shadow-lg hidden" id="sort-dropdown">
                                <div class="rounded-md bg-white shadow-xs">
                                    <div class="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                                        <a href="{{ route('templates.index', ['sort' => 'name', 'search' => request('search'), 'desc' => request('desc')]) }}" class="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900" role="menuitem">
                                            Name
                                        </a>
                                        <a href="{{ route('templates.index', ['sort' => 'keyword', 'search' => request('search'), 'desc' => request('desc')]) }}" class="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900" role="menuitem">
                                            Keyword
                                        </a>
                                        <a href="{{ route('templates.index', ['sort' => 'created_at', 'search' => request('search'), 'desc' => request('desc')]) }}" class="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900" role="menuitem">
                                            Creation Date
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <a href="{{ route('templates.create') }}" class="px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:shadow-outline-indigo focus:border-indigo-700 active:bg-indigo-700 transition duration-150 ease-in-out">New Template</a>
                    </div>
                </div>
                @forelse($templates as $template)
                    <div class="card text-white bg-admin-blue-light overflow-hidden sm:rounded-lg mt-5">
                      <div class="card-body p-2">
                        <h1 class="text-2xl font-bold p-2 pb-0">{{ $template->name }}</h1>
                        <div class="flex items-center justify-between">
                            <div class="flex items-center">
                                <div class="flex p-2">
                                    <svg class="w-6 h-6 mr-1" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 7a2 2 0 012 2m4 0a6 6 0 01-7.743 5.743L11 17H9v2H7v2H4a1 1 0 01-1-1v-2.586a1 1 0 01.293-.707l5.964-5.964A6 6 0 1121 9z"></path></svg>
                                    @if ($template->keyword)
                                        <p> {{ $template->keyword }} </p>
                                    @else
                                        <i> No keyword </i>
                                    @endif
                                </div>
                                <div class="flex align-center p-2">
                                    <svg class="w-6 h-6 mr-1" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 7h.01M7 3h5c.512 0 1.024.195 1.414.586l7 7a2 2 0 010 2.828l-7 7a2 2 0 01-2.828 0l-7-7A1.994 1.994 0 013 12V7a4 4 0 014-4z"></path></svg>
                                    @if ($template->tags->count())
                                        <p>
                                            @foreach ($template->tags as $tag)
                                                {{ $tag->name }} &nbsp;
                                            @endforeach
                                        </p>
                                    @else
                                        <i>No tags</i>
                                    @endif
                                </div>
                            </div>
                            <div class="flex items-center">
                                <a href="{{ route('templates.edit', $template->id) }}" class="px-5 py-2 bg-gray-500 text-white font-bold rounded-lg m-2 flex">
                                    <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"></path></svg>
                                    Edit
                                </a>
                                <a class="px-5 py-2 bg-red-500 text-white font-bold rounded-lg m-2 flex" href="{{ route('templates.destroy', $template->id) }}"
                                    onclick="event.preventDefault();
                                                document.getElementById('destroy-form-{{ $template->id }}').submit();">
                                    <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg>
                                    Delete
                                </a>
                                <form id="destroy-form-{{ $template->id }}" action="{{ route('templates.destroy', $template->id) }}" method="POST" class="d-none">
                                    @csrf
                                    @method("DELETE")
                                </form>
                            </div>
                        </div>
                      </div>
                    </div>
                @empty
                    <div class="bg-white shadow sm:rounded-lg p-5 mt-5">
                        <h6>No entries yet.</h6>
                    </div>
                @endforelse
                <div class="m-3 px-3">
                    {{ $templates->withQueryString()->links('pagination::tailwind') }}
                </div>
            </div>
        </div>

    </div>
@endsection

@push('scripts')
    <script>
        $('#sort').on('click', function() {
            $('#sort-dropdown').toggle('hidden')
        })
    </script>
@endpush
