@extends('layouts.app')

@section('title')
    Create Appointment
@endsection

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
    <div class="flex">
        {{-- Aside --}}
        @include('partials.aside')
        <div class="w-5/6 md:w-4/5 absolute right-0 top-0">
            {{-- Nav --}}
            @include('partials.nav')
            <div class="p-5 md:p-10 z-10 mt-16 bg-admin-gray h-full">
                {{-- Main --}}
                <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                    <div class="px-4 py-5 border-b border-gray-200 sm:px-6">
                        <h3 class="text-xl leading-6 font-medium text-gray-900"> New Appointment </h3>
                        <p class="mt-1 max-w-2xl text-sm leading-5 text-gray-500">
                            Manually schedule an appointment for a client.
                        </p>
                    </div>
                    <form action="{{ route('appointments.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="w-full md:w-5/6 px-4 py-5 border-gray-200 sm:px-6">
                            <div class="w-full md:w-1/2 px-3">
                                <label class="block text-gray-700 font-medium mb-1" for="client_id"> Client <span class="text-red-500 text-md">*</span></label>
                                <select name="client_id" id="client_id" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('client_id') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"></select>
                                @error('client_id')
                                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="w-full md:w-5/6 px-4 py-5 border-gray-200 sm:px-6">
                            <div class="w-full md:w-1/2 px-3">
                                <label class="block text-gray-700 font-medium mb-1" for="type"> Appointment Type <span class="text-red-500 text-md">*</span></label>
                                <div class="relative">
                                    <select name="type" id="type" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('type') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                        <option value="consultation-call" @if (old('type') === null || old('type') === "consultation-call") selected @endif> Consultation Call </option>
                                        <option value="others" @if (old('type') === "others") selected @endif> Others </option>
                                    </select>
                                    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                    </div>
                                </div>
                                @error('type')
                                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="w-full md:w-5/6 px-4 py-5 border-gray-200 sm:px-6">
                            <div class="w-full md:w-1/2 px-3">
                                <label class="block text-gray-700 font-medium mb-1" for="date_time"> Date / Time <span class="text-red-500 text-md">*</span></label>
                                <input type="datetime-local" name="date_time" id="date_time" value="{{ old('date_time') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('date_time') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                @error('date_time')
                                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="w-full md:w-5/6 px-4 py-5 border-gray-200 sm:px-6">
                            <div class="w-full px-3">
                                <label class="block text-gray-700 font-medium mb-1" for="description"> Description </label>
                                <textarea name="description" id="description" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('description') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" cols="30" rows="4">{{ old('description') }}</textarea>
                                <p class="text-gray-600 text-sm mt-2">Brief description for scheduled appointment. URLs are hyperlinked.</p>
                                @error('description')
                                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="flex justify-end bg-gray-100 px-8 py-3">
                            <button type="submit" class="bg-indigo-600 hover:bg-indigo-700 text-white py-2 px-8 rounded-md"> Save </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#client_id').select2({
                ajax: {
                    url: "{{ route('selections.clients') }}",
                    dataType: 'json',
                    processResults: function (data) {
                        return {
                            results: data.results
                        }
                    },
                    delay: 250,
                    cache: true,
                },
                templateResult: function (state) {
                    if (! state.id) {
                        return state.text
                    }
                    return `${state.first_name} ${state.last_name} - ${state.text}`
                },
                templateSelection: function (state) {
                    if (! state.id || ! state.first_name) {
                        return state.text
                    }

                    return `${state.first_name} ${state.last_name}`
                },
                minimumInputLength: 2,
            });
        })
    </script>
@endpush

