@extends('layouts.app')

@section('title')
    Appointments
@endsection

@section('content')
    <div class="flex">

        {{-- Aside --}}
        @include('partials.aside')

        {{-- Content --}}
        <div class="w-5/6 md:w-4/5 absolute right-0 top-0">
            @include('partials.nav')
            <div class="bg-admin-gray @if ($appointments->count()) h-full @else h-screen @endif p-5 md:pt-24 md:pb-10 md:pl-10 md:pr-10 z-10">
                <div class="flex items-center justify-end mb-5">
                    <div>
                        <a href="{{ route('appointments.create') }}" class="px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:shadow-outline-indigo focus:border-indigo-700 active:bg-indigo-700 transition duration-150 ease-in-out">Create New</a>
                    </div>
                </div>
                <div class="flex flex-col">
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                <table class="min-w-full divide-y divide-gray-200">
                                    <tbody class="bg-white divide-y divide-gray-200">
                                        @forelse ($appointments as $appointment)
                                            <tr>
                                                <td class="px-10 py-6 whitespace-no-wrap">
                                                    <div class="flex items-center">
                                                        <div class="flex-shrink-0 h-16 w-16">
                                                            <img class="h-16 w-16 rounded-full" src="{{ $appointment->client->image->url ?? asset('images/placeholder.jpg') }}" alt="{{ $appointment->client->name }}">
                                                        </div>
                                                        <div class="ml-4">
                                                            <div class="text-lg leading-5 font-medium text-gray-900 mb-2">
                                                                {{ ucwords($appointment->client->fullname) }}
                                                            </div>
                                                            <div class="text-sm leading-5 text-gray-600 flex flex-row items-center">
                                                                <svg class="w-6 h-6 mr-1" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"></path></svg>
                                                                {{ $appointment->client->email }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="px-10 py-6 whitespace-no-wrap">
                                                    <div class="text-sm leading-5 text-gray-900">
                                                        {{ $appointment->date_time->toDayDateTimeString() }}
                                                    </div>
                                                    <div class="text-sm leading-5 text-gray-500">
                                                        {{ ucwords($appointment->type) }}
                                                    </div>
                                                </td>
                                                <td class="px-10 py-6 whitespace-no-wrap">
                                                    <div class="text-sm leading-5 text-gray-900">
                                                        <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full {{ $appointment->style_classes }}">
                                                            {{ ucwords($appointment->status) }}
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="px-10 py-6 whitespace-no-wrap">
                                                    <div class="text-sm leading-5 text-gray-900 flex justify-center">
                                                        <a href="{{ route('appointments.show', $appointment->id) }}">
                                                            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td class="px-6 py-4 whitespace-no-wrap" colspan="5">
                                                    <i> No entries yet </i>
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                                @if ($appointments->count())
                                    <div class="m-2 px-3">
                                        {{ $appointments->withQueryString()->links('pagination::tailwind') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#sort').on('click', function() {
            $('#sort-dropdown').toggle('hidden')
        })
    </script>
@endpush
