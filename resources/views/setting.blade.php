@extends('layouts.app')

@section('title')
    Settings
@endsection

@section('content')
    <div class="flex">

        {{-- Aside --}}
        @include('partials.aside')

        {{-- Content --}}
        <div class="w-5/6 md:w-4/5 absolute right-0 top-0">
            @include('partials.nav')
            <div class="p-5 md:p-10 z-10 mt-16 bg-admin-gray h-full">
                <h1 class="text-2xl font-bold">Settings</h1>
                <div class="bg-white shadow overflow-hidden sm:rounded-lg mt-5">
                </div>
            </div>
        </div>

    </div>
@endsection
