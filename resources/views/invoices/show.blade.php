@extends('layouts.app')

@section('title')
    Invoice Information
@endsection

@section('content')
    <div class="flex">
        {{-- Aside --}}
        @include('partials.aside')
        <div class="w-5/6 md:w-4/5 absolute right-0 top-0">
            {{-- Nav --}}
            @include('partials.nav')
            <div class="p-5 md:p-10 z-10 mt-16 bg-admin-gray h-full">
                {{-- Main --}}
                <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                    <div class="flex justify-between items-center border-b border-gray-200 p-8">
                        <div>
                            <h3 class="text-xl leading-6 font-medium text-gray-900"> Invoice Information </h3>
                            <p class="mt-1 max-w-2xl text-sm leading-5 text-gray-500">
                                Invoice details and description.
                            </p>
                        </div>
                        <div>
                            <a href="{{ route('invoices.edit', $invoice->id) }}" class="rounded-lg border border-gray-400 inline-flex items-center py-2 px-5 mr-3">
                                <svg class="w-6 h-6 mr-1" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"></path></svg>
                                <span class="text-lg">Edit</span>
                            </a>
                        </div>
                    </div>
                    <div class="border-b border-gray-200 p-8">
                        Invoice Show
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
