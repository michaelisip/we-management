@extends('layouts.app')

@section('title')
    Invoices
@endsection

@section('content')
    <div class="flex">

        {{-- Aside --}}
        @include('partials.aside')

        {{-- Content --}}
        <div class="w-5/6 md:w-4/5 absolute right-0 top-0">
            @include('partials.nav')
            <div class="bg-admin-gray @if ($invoices->count()) h-full @else h-screen @endif p-5 md:pt-24 md:pb-10 md:pl-10 md:pr-10 z-10">
                <div class="flex items-center justify-end mb-5">
                    <div>
                        <a href="{{ route('invoices.create') }}" class="px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:shadow-outline-indigo focus:border-indigo-700 active:bg-indigo-700 transition duration-150 ease-in-out">Create New</a>
                    </div>
                </div>
                <div class="flex items-center justify-between mb-2">
                    <h1 class="text-2xl font-medium"> Invoice List </h1>
                </div>
                <div class="flex flex-col">
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                <table class="min-w-full divide-y divide-gray-200">
                                    <thead>
                                        <tr>
                                            <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                                Bill Email
                                            </th>
                                            <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                                Last Updated
                                            </th>
                                            <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                                Created
                                            </th>
                                            <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody class="bg-white divide-y divide-gray-200">
                                        @forelse ($invoices as $invoice)
                                            <tr>
                                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                                    {{ $invoice->bill_email }}
                                                </td>
                                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                                    {{ $invoice->last_updated }}
                                                </td>
                                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                                                    {{ $invoice->created }}
                                                </td>
                                                <td class="px-6 py-4 whitespace-no-wrap text-right text-sm leading-5 font-medium">
                                                    <a href="{{ route('invoices.send', $invoice->id) }}" class="text-indigo-600 hover:text-indigo-900">
                                                        Send
                                                    </a>
                                                    <a href="{{ route('invoices.edit', $invoice->id) }}" class="text-indigo-600 hover:text-indigo-900">
                                                        Edit
                                                    </a>
                                                    <a href="{{ route('invoices.void', $invoice->id) }}" class="text-indigo-600 hover:text-indigo-900">
                                                        Void
                                                    </a>
                                                    <a href="{{ route('invoices.destroy', $invoice->id) }}" class="text-indigo-600 hover:text-indigo-900"
                                                        onclick="event.preventDefault();
                                                            document.getElementById('destroy-form-{{ $invoice->id }}').submit();">
                                                        Delete
                                                    </a>
                                                    <form id="destroy-form-{{ $invoice->id }}" action="{{ route('invoices.destroy', $invoice->id) }}" method="POST" class="d-none">
                                                        @csrf
                                                        @method("DELETE")
                                                    </form>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td class="px-6 py-4 whitespace-no-wrap" colspan="5">
                                                    <i> No entries yet </i>
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                                @if ($invoices->count())
                                    <div class="m-2 px-3">
                                        {{ $invoices->withQueryString()->links('pagination::tailwind') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
