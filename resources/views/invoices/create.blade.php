@extends('layouts.app')

@section('title')
    Create Invoice
@endsection

@section('content')
    <div class="flex">
        {{-- Aside --}}
        @include('partials.aside')
        <div class="w-5/6 md:w-4/5 absolute right-0 top-0">
            {{-- Nav --}}
            @include('partials.nav')
            <div class="p-5 md:p-10 z-10 mt-16 bg-admin-gray h-full">
                {{-- Main --}}
                <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                    <div class="px-4 py-5 border-b border-gray-200 sm:px-6">
                        <h3 class="text-xl leading-6 font-medium text-gray-900"> Client Invoice </h3>
                        <p class="mt-1 max-w-2xl text-sm leading-5 text-gray-500">
                            Create invoice for existing client.
                        </p>
                    </div>
                    <form action="{{ route('invoices.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="w-full md:w-5/6 px-4 py-5 border-b border-gray-200 sm:px-6">
                            <div class="w-full md:w-2/3 mb-8">
                                <label class="block text-gray-700 font-medium mb-1" for="service_name"> Service Name <span class="text-red-500 text-md">*</span></label>
                                <input type="text" name="service_name" id="service_name" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('service_name') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" required autofocus>
                                @error('service_name')
                                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="w-full md:w-2/3 mb-8">
                                <label class="block text-gray-700 font-medium mb-1" for="bill_email"> Email address <span class="text-red-500 text-md">*</span></label>
                                <input type="email" name="bill_email" id="bill_email" value="{{ old('bill_email') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('bill_email') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" required>
                                @error('bill_email')
                                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="w-full md:w-2/3 mb-8">
                                <label class="block text-gray-700 font-medium mb-1" for="amount"> Amount <span class="text-red-500 text-md">*</span></label>
                                <input type="number" name="amount" id="amount" value="{{ old('amount') }}" class="appearance-none block w-full text-gray-700 border border-gray-400 @error ('amount') border-red-500 @enderror rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" required>
                                @error('amount')
                                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="flex justify-end bg-gray-100 px-8 py-3">
                            <button type="submit" class="bg-indigo-600 hover:bg-indigo-700 text-white py-2 px-8 rounded-md"> Save </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
