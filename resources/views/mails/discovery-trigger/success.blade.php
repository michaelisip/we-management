@component('mail::message')
# Discovery Call Intake Success

Your inquiry for discovery call intake has been successful

Thanks,<br>
{{ config('app.name') }}
@endcomponent