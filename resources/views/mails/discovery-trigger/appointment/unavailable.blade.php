@component('mail::message')
# Appointment Unavailable

Unfortunately, the date and time on your appointment is not available.

Thanks,<br>
{{ config('app.name') }}
@endcomponent