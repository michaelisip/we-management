@component('mail::message')
# Appointment Available

Your appointment has been set.

Thanks,<br>
{{ config('app.name') }}
@endcomponent