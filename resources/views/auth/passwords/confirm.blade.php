@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Confirm Password') }}</div>

                <div class="card-body">
                    {{ __('Please confirm your password before continuing.') }}

                    <form method="POST" action="{{ route('password.confirm') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Confirm Password') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('title')
    Reset Password
@endsection

@push('head')
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
@endpush

@section('content')
    <div class="bg-admin-gray flex items-center justify-center h-full">
        <form action="{{ route('password.confirm') }}" method="post" class="w-full md:w-1/3">
            @csrf

            <h2>Please confirm your password before continuing</h2>
            @include('partials.messages')
            <input type="hidden" name="token" value="{{ $token }}">

            <div class="bg-white shadow sm:rounded-lg mt-5 py-5 px-10">
                <div class="flex flex-col mb-5">
                    <label for="password" class="font-bold text-gray-700 mb-1 @error('password') border-red-500 @enderror"> Password </label>
                    <input type="password" name="password" class="border-2 rounded  @error('password') border-red-500 @enderror" required autocomplete="current-password">
                    @error('password')
                        <p class="text-red-500 text-xs italic">
                            {{ $message }}
                        </p>
                    @enderror
                </div>
                <div class="flex items-center justify-end">
                    @if (Route::has('password.request'))
                        <a class="inline-block align-baseline font-bold text-sm mr-5" href="{{ route('password.request') }}">
                            Forgot Password?
                        </a>
                    @endif
                    <button class="px-5 py-2 bg-admin-blue-dark text-white font-bold rounded-lg" type="submit">Confirm Password</button>
                </div>
            </div>
        </form>
    </div>
@endsection 
