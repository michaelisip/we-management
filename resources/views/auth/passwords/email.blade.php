@extends('layouts.app')

@section('title')
    Password Reset
@endsection

@section('content')
    <div class="bg-admin-gray flex items-center justify-center h-screen">
        <form action="{{ route('password.email') }}" method="post" class="w-full md:w-1/3">
            @csrf

            <img src="{{ asset('images/logo.png') }}" alt="we Management" class="w-9/12 mx-auto">
            <h1 class="text-center text-gray-900 font-bold text-xl mt-2">Reset account password</h1>

            <div class="bg-white shadow sm:rounded-lg mt-5 p-12">
                <div class="flex flex-col mb-8">
                    <label for="email" class="text-gray-800 mb-1 @error('email') text-red-800 @enderror"> Email Address </label>
                    <input type="text" name="email" class="border-2 rounded py-2 px-4 @error('email') border-red-500 @enderror" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <p class="text-red-500 text-xs italic">
                            {{ $message }}
                        </p>
                    @enderror
                </div>
                <div class="flex flex-col">
                    <button class="bg-blue-500 text-white text-center rounded-lg p-3" type="submit">Send password reset link</button>
                </div>
            </div>
        </form>
    </div>
@endsection
