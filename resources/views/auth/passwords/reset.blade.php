@extends('layouts.app')

@section('title')
    Reset Password
@endsection

@section('content')
    <div class="bg-admin-gray flex items-center justify-center h-screen">
        <form action="{{ route('password.update') }}" method="post" class="w-full md:w-1/3">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <input type="hidden" name="email" value="{{ request('email') }}">

            <img src="{{ asset('images/logo.png') }}" alt="we Management" class="w-9/12 mx-auto">
            <h1 class="text-center text-gray-900 font-bold text-xl mt-2">Type in your new password</h1>

            <div class="bg-white shadow sm:rounded-lg mt-5 p-12">
                {{-- <div class="flex flex-col mb-8">
                    <label for="email" class="text-gray-800 mb-1 @error('email') text-red-800 @enderror"> Email Address </label>
                    <input type="hidden" name="email" class="border-2 rounded py-2 px-4 @error('email') border-red-500 @enderror" value="{{ old('email') ?? request('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <p class="text-red-500 text-xs italic">
                            {{ $message }}
                        </p>
                    @enderror
                </div> --}}
                <div class="flex flex-col mb-8">
                    <label for="password" class="text-gray-800 mb-1 @error('password') text-red-800 @enderror"> Password </label>
                    <input type="password" name="password" class="border-2 rounded py-2 px-4 @error('password') border-red-500 @enderror" required autocomplete="current-password">
                    @error('password')
                        <p class="text-red-500 text-xs italic">
                            {{ $message }}
                        </p>
                    @enderror
                </div>
                <div class="flex flex-col mb-8">
                    <label for="password_confirmation" class="text-gray-800 mb-1 @error('password_confirmation') text-red-800 @enderror"> Confirm Password </label>
                    <input type="password" name="password_confirmation" class="border-2 rounded py-2 px-4 @error('password_confirmation') border-red-500 @enderror" required>
                    @error('password_confirmation')
                        <p class="text-red-500 text-xs italic">
                            {{ $message }}
                        </p>
                    @enderror
                </div>
                <div class="flex flex-col">
                    <button class="bg-blue-500 text-white text-center rounded-lg p-3" type="submit">Reset password</button>
                </div>
            </div>
        </form>
    </div>
@endsection
