@extends('layouts.app')

@section('title')
    Admin Login
@endsection

@section('content')
    <div class="bg-admin-gray flex items-center justify-center h-screen">
        <form action="{{ route('login') }}" method="post" class="w-full md:w-1/3">
            @csrf

            <img src="{{ asset('images/logo.png') }}" alt="we Management" class="w-9/12 mx-auto">
            <h1 class="text-center text-gray-900 font-bold text-3xl">Sign in to your account</h1>

            <div class="bg-white shadow sm:rounded-lg mt-5 p-12">
                <div class="flex flex-col mb-8">
                    <label for="email" class="text-gray-800 mb-1 @error('email') text-red-800 @enderror"> Email Address </label>
                    <input type="text" name="email" class="border-2 rounded py-2 px-4 @error('email') border-red-500 @enderror" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <p class="text-red-500 text-xs italic">
                            {{ $message }}
                        </p>
                    @enderror
                </div>
                <div class="flex flex-col mb-8">
                    <label for="password" class="text-gray-800 mb-1 @error('password') text-red-800 @enderror"> Password </label>
                    <input type="password" name="password" class="border-2 rounded py-2 px-4 @error('password') border-red-500 @enderror" required autocomplete="current-password">
                    @error('password')
                        <p class="text-red-500 text-xs italic">
                            {{ $message }}
                        </p>
                    @enderror
                </div>
                <div class="flex items-center justify-between mb-8">
                    <label class="block">
                        <input class="mr-2 leading-tight" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <span class="text-md">
                            Remember Me
                        </span>
                    </label>
                    @if (Route::has('password.request'))
                        <a class="inline-block align-baseline text-yellow-500" href="{{ route('password.request') }}">
                            Forgot your password?
                        </a>
                    @endif
                </div>
                <div class="flex flex-col">
                    <button class="bg-yellow-500 text-white text-center rounded-lg mb-5 p-3" type="submit">Sign in</button>
                    <a href="{{ route('google.login') }}" class=" bg-red-600 text-white text-center rounded-lg p-3" >Sign in using Google</a>
                </div>
            </div>
        </form>
    </div>
@endsection
