@extends('layouts.app')

@section('title')
    Email Address Verification
@endsection

@section('content')
    <div class="bg-admin-gray flex items-center justify-center h-full">
        <form action="{{ route('verification.resend') }}" method="post" class="w-full md:w-1/3">
            @csrf

            <h2>Before proceeding, please check your email for a verification link</h2>
            @include('partials.messages')

            @if (session('resent'))
                <div class="bg-green-500 shadow sm:rounded-lg mt-5 p-5">
                    <h4 class="font-bold">A fresh verification link has been sent to your email address. </h4>
                </div>
            @endif

            <h2>If you did not receive the email.</h2>
            <div class="bg-white shadow sm:rounded-lg mt-5 py-5 px-10">
                <div class="flex items-center justify-end">
                    <button class="px-5 py-2 bg-admin-blue-dark text-white font-bold rounded-lg" type="submit"> Request Another</button>
                </div>
            </div>
        </form>
    </div>
@endsection 