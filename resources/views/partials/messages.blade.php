@if (session('success'))
    <div class="bg-green-500 shadow sm:rounded-lg mt-5 p-5">
        <h4 class="font-bold">Success! {{ session('success') }}</h4>
    </div>
@endif
@if ($errors->any())
    <div class="bg-yellow-500 shadow sm:rounded-lg mt-5 p-5">
        <h4 class="font-bold">Oops! Something went wrong!</h4>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif