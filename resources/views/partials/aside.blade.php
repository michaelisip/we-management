<aside
    class="w-1/6 md:w-1/5 h-full bg-admin-blue-light text-white fixed z-30"
>
    <a
        href="{{ route('dashboard') }}"
        class="bg-admin-blue-dark flex items-center justify-center h-20 px-5"
    >
        <img src="{{ asset('images/logo.png') }}" alt="We Management System" class="w-full lg:w-10/12">
    </a>
    <ul class="mt-5 px-3">
        {{-- <li>
            <a
                href="{{ route('dashboard') }}"
                class="flex items-center justify-center md:justify-start px-5 py-3 @if (Request::is('dashboard')) bg-admin-blue-dark @endif hover:bg-admin-blue-dark rounded-lg"
            >
                <svg class="w-6 h-6 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"></path></svg>
                <span class="hidden md:flex">Dashboard</span>
            </a>
        </li> --}}
        <li>
            <a
                href="{{ route('clients.index') }}"
                class="flex items-center justify-center md:justify-start px-5 py-3 @if (Request::is('clients', 'clients/*')) bg-admin-blue-dark @endif hover:bg-admin-blue-dark rounded-lg"
            >
                <svg class="w-6 h-6 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4.354a4 4 0 110 5.292M15 21H3v-1a6 6 0 0112 0v1zm0 0h6v-1a6 6 0 00-9-5.197M13 7a4 4 0 11-8 0 4 4 0 018 0z"></path></svg>
                <span class="hidden md:flex">Clients</span>
            </a>
        </li>
        <li>
            <a
                href="{{ route('appointments.index') }}"
                class="flex items-center justify-center md:justify-start px-5 py-3 @if (Request::is('appointments', 'appointments/*')) bg-admin-blue-dark @endif hover:bg-admin-blue-dark rounded-lg"
            >
                <svg class="w-6 h-6 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"></path></svg>
                <span class="hidden md:flex">Appointments</span>
            </a>
        </li>

        {{-- <li>
            <a
                href="{{ route('asana') }}"
                class="flex items-center justify-center md:justify-start px-5 py-3 @if (Request::is('asana')) bg-admin-blue-dark @endif hover:bg-admin-blue-dark rounded-lg"
            >
                <svg class="w-6 h-6 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2"></path></svg>
                <span class="hidden md:flex">Asana</span>
            </a>
        </li> --}}

        <li>
            <a
                href="{{ route('invoices.index') }}"
                class="flex items-center justify-center md:justify-start px-5 py-3 @if (Request::is('invoices', 'invoices/*')) bg-admin-blue-dark @endif hover:bg-admin-blue-dark rounded-lg"
            >
                <svg class="w-6 h-6 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z"></path></svg>
                <span class="hidden md:flex">Invoices</span>
            </a>
        </li>

        <li>
            <a
                href="{{ route('templates.index') }}"
                class="flex items-center justify-center md:justify-start px-5 py-3 @if (Request::is('templates', 'templates/*')) bg-admin-blue-dark @endif hover:bg-admin-blue-dark rounded-lg"
            >
                <svg class="w-6 h-6 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"></path></svg>
                <span class="hidden md:flex">Email Templates</span>
            </a>
        </li>
        <li>
            <a
                href="{{ route('users.index') }}"
                class="flex items-center justify-center md:justify-start px-5 py-3 @if (Request::is('users', 'users/*')) bg-admin-blue-dark @endif hover:bg-admin-blue-dark rounded-lg"
            >
                <svg class="w-6 h-6 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"></path></svg>
                <span class="hidden md:flex">Users</span>
            </a>
        </li>
        <li>
            <a
                href="{{ route('discovery.form') }}"
                class="flex items-center justify-center md:justify-start px-5 py-3 @if (Request::is('discovery-form')) bg-admin-blue-dark @endif hover:bg-admin-blue-dark rounded-lg"
            >
                <svg class="w-6 h-6 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"></path></svg>
                <span class="hidden md:flex">Discovery Form</span>
            </a>
        </li>
        {{-- <li>
            <a
                href="{{ route('settings.index') }}"
                class="flex items-center justify-center md:justify-start px-5 py-3 @if (Request::is('settings')) bg-admin-blue-dark @endif hover:bg-admin-blue-dark rounded-lg"
            >
                <svg class="w-6 h-6 mr-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"></path><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path></svg>
                <span class="hidden md:flex">Settings</span>
            </a>
        </li> --}}
    </ul>
</aside>
