<nav
    class="flex items-center justify-between shadow fixed w-full h-16 px-5 right-0 z-20 bg-white"
>
    <div class="w-1/6 md:w-1/5"></div>
    <div class="w-4/6 md:w-3/5 md:flex md:items-center justify-start">
        <div class="flex-grow">
            <input class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white" type="text" id="search-suggestions" name="keyword" placeholder="Search...">
        </div>
    </div>
    <form
        action="{{ route('logout') }}"
        method="post"
        class="w-1/6 md:w-1/5 flex-none flex justify-end"
    >
        @csrf
        <button>Logout</button>
    </form>
</nav>

@push('scripts')
    <script>
        $(document).ready(function() {
            $("#search-suggestions").keyup(function(){
                var keyword = $(this).val()
                if (keyword.length > 2) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('search.suggestions') }}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "keyword": keyword
                        },
                        success: function(data){
                            console.log(data)
                            // TODO: append html for search suggestions
                        }
                    })
                }
            })
        })
    </script>
@endpush
