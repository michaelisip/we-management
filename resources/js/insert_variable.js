/**
 * A plugin to enable placeholder tokens to be inserted into the CKEditor message. Use on its own or with teh placeholder plugin. 
 * The default format is compatible with the placeholders syntex
 *
 * @version 0.1 
 * @Author Troy Lutton
 * @license MIT 
 * 
 * This is a pure modification for the placeholders plugin. All credit goes to Stuart Sillitoe for creating the original (stuartsillitoe.co.uk)
 *
 */

CKEDITOR.plugins.add('insert_variable',
{
	requires : ['richcombo'],
	init : function( editor )
	{
		//  array of variables to choose from that'll be inserted into the editor
		var variables = [];
		
		// init the default config - empty variables
		var defaultConfig = {
			format: '{{%variable%}}',
			variables : []
		};

		// merge defaults with the passed in items		
		var config = CKEDITOR.tools.extend(defaultConfig, editor.config.insert_variable || {}, true);

		// run through an create the set of items to use
		for (var i = 0; i < config.variables.length; i++) {
			// get our potentially custom variable format
			var variable = config.format.replace('%variable%', config.variables[i]);			
			variables.push([variable, config.variables[i], config.variables[i]]);
		}

		// add the menu to the editor
		editor.ui.addRichCombo('insert_variable',
		{
			label: 		'Variable',
			title: 		'Variable',
			voiceLabel: 'Variable',
			className: 	'cke_format',
			multiSelect:false,
			panel:
			{
				css: [ editor.config.contentsCss, CKEDITOR.skin.getPath('editor') ],
				voiceLabel: editor.lang.panelVoiceLabel
			},

			init: function()
			{
				this.startGroup( "Insert variable" );
				for (var i in variables)
				{
					this.add(variables[i][0], variables[i][1], variables[i][2]);
				}
			},

			onClick: function( value )
			{
				editor.focus();
				editor.fire( 'saveSnapshot' );
				editor.insertHtml(value);
				editor.fire( 'saveSnapshot' );
			}
		});
	}
});