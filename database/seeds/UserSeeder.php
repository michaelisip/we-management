<?php

use App\Role;
use App\User;

use Faker\Factory;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $data = [];

        for ($i=0; $i < 50; $i++) {
            $firstName = $faker->firstName;

            $data[] = [
                'first_name' => $firstName,
                'last_name' => $faker->boolean(50) ? $faker->lastName : null,
                'email' => Str::of($firstName)->replace(' ', '') . $i . '@' . $faker->domainName,
                'email_verified_at' => $faker->boolean(70) ? null : now(),
                'password' => 'password',
                'updated_at' => now()->toDateTimeString(),
                'created_at' => now()->toDateTimeString(),
            ];
        }

        foreach (array_chunk($data, 100) as $chunk) {
            User::insert($chunk);
        }

        $this->assignRoles();
    }

    public function assignRoles()
    {
        $faker = Factory::create();

        foreach (User::all() as $user) {
            $user->roles()->attach($faker->randomElements([1, 2, 3]));
        }
    }

}
