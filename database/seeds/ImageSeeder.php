<?php

use App\User;
use App\Image;
use App\Client;

use Faker\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->modelImage(new Client);
        $this->modelImage(new User);
    }

    public function modelImage($model)
    {
        $faker = Factory::create();

        $data = [];
        $ids = collect($model->all()->modelKeys());

        foreach ($ids as $id) {
            $data[] = [
                'filename' => $model->find($id)->first_name,
                'url' => $faker->imageUrl(),
                'imageable_type' => $model instanceof Client ? 'App\Client' : 'App\User',
                'imageable_id' => $id,
                'updated_at' => now()->toDateTimeString(),
                'created_at' => now()->toDateTimeString(),
            ];
        }

        foreach (array_chunk($data, 10) as $chunk) {
            Image::insert($chunk);
        }
    }
}
