<?php

use App\Client;
use App\Appointment;

use Faker\Factory;
use Illuminate\Database\Seeder;

class AppointmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $data = [];
        $clients = collect(Client::all()->modelKeys());

        for ($i=0; $i < 50; $i++) {
            $status = $faker->randomElement(['pending', 'done', 'cancelled']);

            $data[] = [
                'client_id' => $clients->random(),
                'type' => $faker->randomElement(['consultation-call', 'others']),
                'date_time' => $status === 'done' ? now()->subHours($faker->numberBetween(10, 1000)) : now()->addMinutes($faker->randomDigit)->addHours($faker->numberBetween(1, 1000)),
                'description' => $faker->boolean(50) ? $faker->sentence : null,
                'status' => $status,
                'cancelled_at' => $status === 'cancelled' ? now() : null,
                'updated_at' => now()->toDateTimeString(),
                'created_at' => now()->toDateTimeString(),
            ];
        }

        foreach (array_chunk($data, 100) as $chunk) {
            Appointment::insert($chunk);
        }
    }
}
