<?php

use App\User;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class SuperadminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'first_name' => 'Superadmin',
            'last_name' => 'User',
            'email' => 'superadmin@wms.com',
            'email_verified_at' => now(),
            'password' => 'password' // hashed on saved from model
        ]);

        $user->roles()->attach(1);
    }
}
