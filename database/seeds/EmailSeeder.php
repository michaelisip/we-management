<?php

use App\Email;
use App\Client;

use Faker\Factory;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class EmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $data = [];
        $clientIds = collect(Client::all()->modelKeys());

        foreach ($clientIds as $id) {
            $data[] = [
                'email' => Str::of($faker->userName)->replace(' ', '') . $id . '@' . $faker->domainName,
                'emailable_type' => 'App\Client',
                'emailable_id' => $id,
                'updated_at' => now()->toDateTimeString(),
                'created_at' => now()->toDateTimeString(),
            ];
        }

        foreach (array_chunk($data, 10) as $chunk) {
            Email::insert($chunk);
        }
    }
}
