<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // live seeders
        $this->call([
            RoleSeeder::class,
            SuperadminSeeder::class,
        ]);

        // local and staging seeders
        $this->call([
            ClientSeeder::class,
            AppointmentSeeder::class,
            UserSeeder::class,
            ImageSeeder::class,
            // EmailSeeder::class,
        ]);
    }
}
