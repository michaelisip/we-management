<?php

use App\Client;

use Faker\Factory;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $data = [];

        for ($i=0; $i < 50; $i++) {

            $firstName = $faker->firstName;
            $status = $faker->randomElement(['archived', 'active', 'inactive']);

            $data[] = [
                'type' => $faker->randomElement(['broker', 'public']),
                'first_name' => $firstName,
                'last_name' => $faker->boolean(50) ? $faker->lastName : null,
                'middle_name' => $faker->boolean(50) ? $faker->lastName : null,
                'suffix' => $faker->boolean(50) ? $faker->suffix : null,
                'email' => strtolower(Str::of($firstName)->replace(' ', '')) . $i . '@' . $faker->domainName,
                'status' => $status,
                'archived_at' => $status === 'archived' ? now() : null,
                'updated_at' => now()->toDateTimeString(),
                'created_at' => now()->toDateTimeString(),
                'company_name' => $faker->boolean(50) ? $faker->company : null,
                'job_title' => $faker->boolean(50) ? $faker->jobTitle : null,
                'website_link' => $faker->boolean(50) ? $faker->url : null,
                'background_info' => $faker->boolean(50) ? $faker->paragraph : null,
                'birthday' => $faker->boolean(50) ? $faker->date() : null,
                'timezone' => $faker->boolean(50) ? $faker->timezone : null,
            ];
        }

        foreach (array_chunk($data, 100) as $chunk) {
            Client::insert($chunk);
        }
    }
}
