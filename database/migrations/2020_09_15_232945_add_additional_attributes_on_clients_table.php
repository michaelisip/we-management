<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdditionalAttributesOnClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->string('middle_name')->nullable()->after('last_name');
            $table->string('suffix')->nullable()->after('middle_name');
            $table->string('company_name')->nullable();
            $table->string('job_title')->nullable();
            $table->string('website_link')->nullable();
            $table->longText('background_info')->nullable();
            $table->date('birthday')->nullable();
            $table->string('timezone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
