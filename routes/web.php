<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['throttle:60,1'])->namespace('Auth')->group(function() {
    Route::get('/login/google', 'LoginController@google')->name('google.login');
    Route::get('/login/google/callback', 'LoginController@googleCallback')->name('google.callback');
});

Route::redirect('/', '/login');

Route::get('dashboard', 'DashboardController@index')->name('dashboard');
Route::post('search', 'DashboardController@suggestions')->name('search.suggestions');

Route::get('asana', 'AsanaServiceController')->name('asana');

Route::resource('appointments', 'AppointmentController');
Route::resource('clients', 'ClientController');
Route::resource('users', 'UserController');
Route::resource('settings', 'SettingController');

Route::prefix('')->group(function() {
    Route::resource('invoices', 'InvoiceController');
    Route::get('quickbooks/invoice', 'InvoiceController@invoice')->name('invoices.quickbooks');
    Route::get('quickbooks/invoice/callback', 'InvoiceController@invoiceCallback')->name('invoices.quickbooks.callback');
    Route::get('quickbooks/invoice/{invoice}/void', 'InvoiceController@void')->name('invoice.void');
    Route::get('quickbooks/invoice/{invoice}/sendemail', 'InvoiceController@sendEmail')->name('invoice.send');
});

Route::prefix('')->group(function() {
    Route::resource('templates', 'TemplateController');
    Route::get('/{template}/fill', 'TemplateController@fill')->name('templates.fill');
    Route::post('/ckeditor/upload', 'TemplateController@uploadImage')->name('templates.ckeditor.upload');
});

Route::name('mail.')->prefix('mail')->group(function() {
    Route::get('/', 'MailController@compose')->name('compose');
    Route::post('/send', 'MailController@send')->name('send');
});

Route::put('clients/{client}/archive', 'ClientController@archive')->name('clients.archive');
Route::put('appointments/{appointment}/done', 'AppointmentController@done')->name('appointments.done');
Route::put('appointments/{appointment}/cancel', 'AppointmentController@cancel')->name('appointments.cancel');

Route::name('selections.')->prefix('selections')->group(function() {
    Route::get('tags', 'SelectController@tags')->name('tags');
    Route::get('clients', 'SelectController@clients')->name('clients');
    Route::get('templates', 'SelectController@templates')->name('templates');
    Route::get('groups', 'SelectController@groups')->name('groups');
    Route::get('roles', 'SelectController@roles')->name('roles');
});

// Temporary routes for discovery form
Route::view('discovery-form', 'discovery-form')->name('discovery.form');
Route::post('/discovery-call-intake', 'API\DiscoveryController@trigger')->name('discovery.trigger');
