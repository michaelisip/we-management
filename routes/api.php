<?php

use App\Http\Middleware\VerifyDiscovery;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::middleware(VerifyDiscovery::class)->group(function() {
    // Commented temporarily for discovery form testing
    // Route::post('/discovery-call-intake', 'DiscoveryController@trigger')->name('discovery.trigger');
// });
