-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2020 at 02:50 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `we_management_system_local`
--

-- --------------------------------------------------------

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`name`, `deleted_at`, `created_at`, `updated_at`) VALUES
('Update', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
('Create', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
('Unavailable', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
('Available', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
('Consultation', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
('Followups', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

-- --------------------------------------------------------

--
-- Dumping data for table `tag_template`
--

INSERT INTO `tag_template` (`tag_id`, `template_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 14, NULL, NULL, NULL),
(5, 20, NULL, NULL, NULL),
(6, 25, NULL, NULL, NULL),
(1, 28, NULL, NULL, NULL),
(1, 1, NULL, NULL, NULL),
(1, 2, NULL, NULL, NULL),
(1, 5, NULL, NULL, NULL),
(1, 7, NULL, NULL, NULL),
(1, 11, NULL, NULL, NULL),
(1, 29, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Dumping data for table `templates`
--

INSERT INTO `templates` (`name`, `keyword`, `body`, `deleted_at`, `created_at`, `updated_at`, `subject`, `to`, `cc`, `bcc`) VALUES
('3 Bureau Issues / Upgrade', 'bureau', '<p>Hello {{To: First Name}},</p>\r\n\r\n<p>We would love to provide you with an update. However, your <strong><span class=\"marker\">{Credit Monitoring}</span></strong> membership plan does not give us access.</p>\r\n\r\n<p>We highly recommend the plan that could give us access to view your monthly 3 Bureau Report to track your progress.</p>\r\n\r\n<p>Please resolve the matter and we will get you your update asap.</p>\r\n\r\n<p>Thank You,</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Please update Membership Plan for {Credit Monitoring}', NULL, NULL, '[\"882DF@logemail.lessannoyingcrm.com\"]'),
('CCT/EX Missing', 'missing', '<p>Greetings&nbsp;{{To: First Name}}&nbsp;</p>\r\n\r\n<p>How are you? We are not able to access your credit monitoring login with the information we have on file. Can you please send us your Username and Password at your earliest opportunity?</p>\r\n\r\n<p>The information we have on file is: <span class=\"marker\"><strong>{Credit Monitoring}</strong></span></p>\r\n\r\n<p>Username: <span class=\"marker\">{Username}</span></p>\r\n\r\n<p>Password is: <span class=\"marker\">{Password}</span></p>\r\n\r\n<p>Also, please send your current <span class=\"marker\"><ins>Security Answer</ins></span> and <span class=\"marker\"><ins>PIN</ins></span>.</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Correct Login for {Credit Monitoring}', NULL, NULL, NULL),
('Credit Karma', 'karma', '<p>Greetings!</p>\r\n\r\n<p>In order to provide you with an accurate update we need to see your 3 bureau report every 30 - 45 days. Credit Karma only shows 2 of 3 bureaus making it ineligible to give you an 100% accurate update report.</p>\r\n\r\n<p>We solely utilize Credit Karma as an accessory to better support you. However, it should not take the place of your paid Credit Monitoring which provides a 3 bureau report.&nbsp;</p>\r\n\r\n<p>Please reinstate a paid membership and let us know when it is accessible.&nbsp;</p>\r\n\r\n<p>Regards,</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL),
('Credit KARMA Credit Card', 'karmacard', '<p>Greetings,</p>\r\n\r\n<p>Credit Karma&#39;s job is to always recommend cword:redit cards, whether you truly qualify or not.&nbsp;</p>\r\n\r\n<p>For us to support you about when cards should be added to your credit profile; it requires a scheduled consultation as we want to dedicate time to the process and not making decisions on the spot.</p>\r\n\r\n<p>Please secure an appointment if you desire our support in this step. The 15 min $40.00 should be enough time.</p>\r\n\r\n<p>We look forward to assisting you and think you should book the session when your next update arrives for the most accuracy.</p>\r\n\r\n<p>Regards,</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL),
('Credit Update Delay', 'delay', '<p>Hello {{To: First Name}},</p>\r\n\r\n<p>How are you?&nbsp;</p>\r\n\r\n<p>This message is to inform you that there will be a delay with your credit update.</p>\r\n\r\n<p>Why?</p>\r\n\r\n<p>The next update will be on <span class=\"marker\">{Next update}</span> because that&#39;s when your credit monitoring will give us access. Please expect to receive your progress on or after the <span class=\"marker\">{Next update}</span>.</p>\r\n\r\n<p>Thanks!</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Credit Update Delay', NULL, NULL, NULL),
('Encourage Karma', 'enckarma', '<p>We would like to encourage you to sign up with <a href=\"https://creditkarma.com/signup\">Credit Karma</a>. We solely utilize Credit Karma as an accessory to better support you. However, it should not take the place of your paid Credit Monitoring which provides a 3 Bureau Report.</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL),
('Expired Membership', 'expired', '<p>Hello {{To: First Name}},</p>\r\n\r\n<p>We would love to provide you with an update. However, your <span class=\"marker\"><strong>{Credit Monitoring}</strong></span> <span class=\"marker\"><ins>membership has expired</ins></span>. Please resolve the matter and we will get you your update asap.</p>\r\n\r\n<p>Thank You,</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Cancelled / Expired Membership for {Credit Monitoring}', NULL, NULL, NULL),
('Interested to Add Dispute', NULL, '<p>Hi {{To: First Name}},</p>\r\n\r\n<p>Thank You for your message. Marshell would like to speak with you to ensure she is giving the best support.</p>\r\n\r\n<p>Please select the best date/time to speak with her.</p>\r\n\r\n<p><a href=\"https://calendly.com/marshellwilliams/free-support-phone-call?month=2020-09\">BOOK HERE</a>!</p>\r\n\r\n<p>Regards,</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL),
('Invalid CCT/EX', 'invalidcct', '<p>Greetings {{To: First Name}}</p>\r\n\r\n<p>How are you? We are not able to access your credit monitoring login with the information we have on file. Can you please send us your current credentials at your earliest opportunity?</p>\r\n\r\n<p>The information we have on file is: <span class=\"marker\"><strong>{Credit Monitoring}</strong></span></p>\r\n\r\n<p>Username: <span class=\"marker\">{Username}</span></p>\r\n\r\n<p>Password: <span class=\"marker\">{Password}</span></p>\r\n\r\n<p>Also, please send your current <strong><ins><span class=\"marker\">Security Answer</span></ins></strong> and <strong><ins><span class=\"marker\">PIN</span></ins></strong>.</p>\r\n\r\n<p>Thanks,</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Correct Login for {Credit Monitoring}', NULL, NULL, NULL),
('Invalid Credit Karma', 'invalidck', '<p>Meanwhile, we can pull up your Credit Karma Credit Report. However, it is not giving us access.</p>\r\n\r\n<p>Can you please send us your current Credit Karma credentials at your earliest opportunity? &nbsp;</p>\r\n\r\n<p>Below is the information we have on file:</p>\r\n\r\n<p>Username: <span class=\"marker\">{Username}</span></p>\r\n\r\n<p>Password: <span class=\"marker\">{Password}</span></p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL),
('Invalid IDIQ-PG', 'invalididiq', '<p>Greetings {{To: First Name}},</p>\r\n\r\n<p>How are you? We are not able to access your credit monitoring login with the information we have on file. Can you please send us your current credentials at your earliest opportunity?</p>\r\n\r\n<p>The information we have on file is: <strong><span class=\"marker\">{Credit Monitoring}</span></strong></p>\r\n\r\n<p>Username: <span class=\"marker\">{Username}</span></p>\r\n\r\n<p>Password: <span class=\"marker\">{Password}</span></p>\r\n\r\n<p>Thanks,</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Correct Login for {Credit Monitoring}', NULL, NULL, NULL),
('LenCred - Don\'t need Credit Karma', NULL, '<p>Thank you so much for your response.&nbsp;</p>\r\n\r\n<p>In order to perform your service, we would need access to your 3 Bureau Credit Report. If we don&#39;t, we would need to stop your service as we would have no way of truly tracking your progress.&nbsp;</p>\r\n\r\n<p>Should you change your mind, and reinstate your Credit Monitoring, we will happily continue our work.</p>\r\n\r\n<p>Additionally, we will notify LenCred of your preference. Have a great day!</p>\r\n\r\n<p>Respectfully,</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, NULL, '[\"smory@lencred.com\"]', NULL),
('Marshell on Appointment', 'marshell', '<p>Hi!</p>\r\n\r\n<p>Marshell is in appointments, but I will definitely get one of our team members to call you and hear your needs. If we are unable to answer your questions on the spot we will happily bring them back to Marshell for support and get you your answers, today.</p>\r\n\r\n<p>See you soon!</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL),
('Membership Issues', 'membership', '<p>Hello {{To: First Name}},</p>\r\n\r\n<p>We would love to provide you with an update. However, there is an issue with your Experian membership plan. Please resolve the matter by contacting their membership support.</p>\r\n\r\n<p>Thank You,</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Please contact Membership Support', NULL, NULL, NULL),
('Missing Security Answer', 'missinganswer', '<p>Greetings!</p>\r\n\r\n<p>We are trying to produce your update however, we need your Credit Monitoring&#39;s Security Answer on file.&nbsp;</p>\r\n\r\n<p>Please send us your Security Answer at your earliest convenience.</p>\r\n\r\n<p>Thanks!</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'URGENT: Please Send Us Your Security Answer', NULL, NULL, NULL),
('No Change', 'nochange', '<p>Your accounts are not removing because we don&#39;t have access to your Credit Monitoring. Your account has been inactive shortly after you have begun services. There have been multiple emails from us sharing this information with you.</p>\r\n\r\n<p>Please understand that without your access to your login, we wouldn&#39;t have access to track your progress. Therefore, if you would like us to continue working on your credit profile, please reinstate your login so we can start asap.</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL),
('No CM interest?', 'nohear', '<p>Greetings!</p>\r\n\r\n<p>How are you?</p>\r\n\r\n<p>We are checking in to see if you still have an interest in our services.&nbsp;</p>\r\n\r\n<p>If yes, please send your Credit Monitoring credentials so we can gain access to your current 3 Bureaus report.&nbsp;</p>\r\n\r\n<p>If no, please make us aware so we don&#39;t inconvenience you by contacting you.&nbsp;</p>\r\n\r\n<p>Have a great day!</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Are you still interested in Credit Repair?', NULL, NULL, NULL),
('No Contact ever', NULL, '<p>I apologize for any confusion. However, our records show that we contacted you via email regarding your <span class=\"marker\">{issue}</span> preventing us from gaining access to your Credit Monitoring. To alleviate further confusion, may we contact you via telephone to rectify this matter and proceed with your service.&nbsp;</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL),
('Not Deleted?', 'notdeleted', '<p>Hi {{To: First Name}},</p>\r\n\r\n<p>How are you?</p>\r\n\r\n<p>We are saddened too. :(&nbsp;</p>\r\n\r\n<p>We want you to receive amazing results. Removals are always at the discretion of the Credit Bureaus and have been delayed because of COVID.&nbsp;</p>\r\n\r\n<p>However, on a positive note, we are aware of your concern and are actively working on solutions.</p>\r\n\r\n<p>Thank you for your patience.</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Lack of Removal Concerns', NULL, NULL, NULL),
('Preparation for Consultation', 'prepconsult', '<p>Greetings!</p>\r\n\r\n<p>In preparation for the upcoming consultation, please see your details and fees. We will discuss on the call.</p>\r\n\r\n<p>See you soon!</p>\r\n\r\n<p>Regards,</p>\r\n\r\n<p>Jai M.</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Credit Report with Fees Attached', NULL, NULL, NULL),
('Produce Update', 'produce', '<p>Hi Angel,&nbsp;</p>\r\n\r\n<p>Please produce this client&#39;s update</p>\r\n\r\n<p>Thank you!</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, NULL, '[\"support@wemanagementinc.com\"]', NULL),
('Score not updating', 'score', '<p>Hi {{To: First Name}},</p>\r\n\r\n<p>We recently performed your update and discovered that your {Bureau} score is missing.&nbsp;</p>\r\n\r\n<p>Please contact <span class=\"marker\">{Bureau}</span> directly and request that they provide you with the reason why your score is not showing.</p>\r\n\r\n<p>Let us know once you get this sorted and/or if you have any additional questions.&nbsp;</p>\r\n\r\n<p><span class=\"marker\">{Screenshot}</span></p>\r\n\r\n<p>Thanks,</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Please contact {Bureau}, your score is not showing', NULL, NULL, NULL),
('Sent Letters no CM', 'letters', '<p>Hi {{To: First Name}},</p>\r\n\r\n<p>Greetings! How are you?</p>\r\n\r\n<p>We are reaching out to gain access to your Credit Monitoring. We were able to produce your new letters and able to perform your update. However, the login we had previously does not give us access and will slow down your progress. Ofcourse! We don&rsquo;t want that to happen. So we are communicating early so it can be fixed.</p>\r\n\r\n<p>Please resolve the matter by sending us the correct login so we can gain entry.&nbsp;</p>\r\n\r\n<p>Send the following:</p>\r\n\r\n<ol>\r\n	<li>Name of your credit monitoring platform: (Identity IQ, My FICO, Experian, etc.)</li>\r\n	<li>Username</li>\r\n	<li>Password</li>\r\n	<li>* Any relevant security questions/answers</li>\r\n</ol>\r\n\r\n<p>Regards,</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Your Credit Monitoring Is Not Giving Us Access', NULL, NULL, NULL),
('Sheila Template 2', 'shiitemp2', '<p>Greetings!</p>\r\n\r\n<p>I would like to check with you again if you have seen this email yet?</p>\r\n\r\n<p>Please let me know if you have any questions :)</p>\r\n\r\n<p>Regards,</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL),
('Shiela Follow Up Template', 'shiitemp', '<p>Greetings!</p>\r\n\r\n<p>I would like to follow up if you have seen my last email yet?</p>\r\n\r\n<p>Please see my previous email on this thread.&nbsp;</p>\r\n\r\n<p>Let me know if you have any questions :)</p>\r\n\r\n<p>Thank you so much!</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL),
('Sign Up Identity IQ', 'regidiq', '<p>I would like to encourage you to sign up with <strong>Identity IQ</strong> instead. &nbsp;</p>\r\n\r\n<p>How do I gain access to ID IQ? Use this link: <a href=\"https://bit.ly/2SpDYYR\">Click The Link Here</a></p>\r\n\r\n<p>MOST IMPORTANT PLEASE FOLLOW INSTRUCTIONS ON HOW TO SET UP YOUR Identity IQ LOGIN</p>\r\n\r\n<p>When setting up your account, please follow the recommended login request to ensure that your service is the most efficient</p>\r\n\r\n<p>Username: <span class=\"marker\">Your email</span> (Please use the email you signed up with us) &nbsp;</p>\r\n\r\n<p>Password: <span class=\"marker\">Capital123 (Capital C)</span> <strong>(Please use this password)</strong> &nbsp;</p>\r\n\r\n<p>Last 4 of your social - <strong>send back the last 4 of your social</strong> security number. it&#39;s needed for me to gain access</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL),
('tempshii3', 'tempshii3', '<p>Greetings!</p>\r\n\r\n<p>We have been contacting you for a while.</p>\r\n\r\n<p>Please respond to this email if you got this or need help.</p>\r\n\r\n<p>Thank you so much!</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL),
('Too Early for an Update', 'early', '<p>Greetings!</p>\r\n\r\n<p>Thanks for reaching out! Based on our system, it&#39;s still too early for your update. The last letter sent was on <span class=\"marker\">07/03/2020</span>, so, the next update that we can expect is around <span class=\"marker\">08/17/2020</span>.</p>\r\n\r\n<p>We will share the update when it is available. Thank you!</p>\r\n\r\n<p>Regards,</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Too Early for An Update', NULL, NULL, NULL),
('Update Credit Card', 'card', '<p>Hello {{To: First Name}},</p>\r\n\r\n<p>We would like to prepare the most recent update of your Credit Report. We have been unsuccessful at this time due to your Credit Card Information with <span class=\"marker\"><strong>{Credit Monitoring}</strong></span>. Please update your Credit Card information.</p>\r\n\r\n<p>Thank You,</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Please update Credit Card Information for {Credit Monitoring}', NULL, NULL, NULL),
('Why Billed?', 'pay', '<p>Greetings!</p>\r\n\r\n<p>You are being billed monthly for your credit monitoring.</p>\r\n\r\n<p><strong>Why?</strong> - because it&#39;s the way we track your credit progress. We login into the <span class=\"marker\"><strong>Credit Monitoring</strong></span> account to confirm which items remain or remove on your credit profile. Therefore, please reinstate your membership until your service is complete</p>\r\n\r\n<p>Thank You</p>', NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL);

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
