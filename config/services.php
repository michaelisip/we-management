<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'google' => [    
        'client_id' => env('GOOGLE_CLIENT_ID'),  
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),  
        'redirect' => env('GOOGLE_REDIRECT_URI')
    ],

    'quickbooks' => [
        'auth_mode' => env('QUICKBOOKS_AUTH_MODE', 'oauth2'),
        'client_id' => env('QUICKBOOKS_CLIENT_ID'),
        'client_secret' => env('QUICKBOOKS_CLIENT_SECRET'),
    ],

    'asana' => [
        'acces_code' => env('ASANA_PERSONAL_ACCESS_TOKEN'),
    ],

    'lessannoying' => [
        'user_code' => env('LESS_ANNOYING_USER_CODE'),
        'api_token' => env('LESS_ANNOYING_API_TOKEN'),
    ],

    'activecampaign' => [
        'base_url' => env('ACTIVE_CAMPAIGN_BASE_URL'),
        'api_key' => env('ACTIVE_CAMPAIGN_API_KEY'),
    ],
];
