<?php

namespace App;

use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model implements Searchable
{
    use SoftDeletes;

    protected $fillable = [
        'type', 'first_name', 'last_name', 'middle_name', 'suffix', 'email', 'status', 'birthday',
        'company_name', 'job_title', 'website_link', 'timezone', 'background_info', 'archived_at'
    ];

    protected $dates = [
        'archived_at',
        'deleted_at',
        'updated_at',
        'created_at',
    ];

    protected $appends = [
        'fullname'
    ];

    public function appointments()
    {
        return $this->hasMany('App\Appointment');
    }

    public function attributes()
    {
        return $this->morphMany('App\Attribute', 'attributable');
    }

    public function files()
    {
        return $this->morphMany('App\File', 'fileable');
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group');
    }

    public function socials()
    {
        return $this->morphMany('App\Social', 'sociable');
    }

    public function phones()
    {
        return $this->morphMany('App\Phone', 'phoneable');
    }

    public function addresses()
    {
        return $this->morphMany('App\Address', 'addressable');
    }

    public function notes()
    {
        return $this->morphMany('App\Note', 'noteable');
    }

    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }

    public function getFullnameAttribute()
    {
        return "{$this->first_name} " . "{$this->middle_name} " . "{$this->last_name}" . " {$this->suffix}";
    }

    public static function types() : array
    {
        return [
            'broker',
            'public',
        ];
    }

    public static function statuses() : array
    {
        return [
            'active',
            'inactive',
            'archived',
        ];
    }

    public function getSearchResult() : SearchResult
    {
        $url = route('clients.show', $this->id);

        return new SearchResult(
            $this,
            $this->fullname,
            $url,
        );
    }

    public function getStyleClassesAttribute()
    {
        if ($this->status === 'active') {
            return 'bg-green-100 text-green-800';
        } elseif ($this->status === 'archived') {
            return 'bg-orange-200 text-orange-800';
        } else {
            return 'bg-red-200 text-red-800';
        }
    }
}
