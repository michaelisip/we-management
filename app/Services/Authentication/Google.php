<?php

namespace App\Services\Authentication;

use App\User;
use App\SocialAuth;

use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Contracts\User as UserContract;

class Google
{
    public function createOrGetUser(UserContract $providerUser)
    {
        $googleAccount = SocialAuth::whereProvider('google')
            ->whereProviderId($providerUser->getId())
            ->first();

        if (isset($googleAccount)) {
            return $googleAccount->user;
        } else {
            $newGoogleAccount = new SocialAuth([
                'provider_id' => $providerUser->getId(),
                'provider' => 'google',
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (is_null($user)) {
                $user = User::create([
                    'first_name' => $providerUser->getName(),
                    'email' => $providerUser->getEmail(),
                    'email_verified_at' => now(),
                    'password' => Hash::make(rand(1, 10000)),
                ]);

                // set role to affiliate
                $user->roles()->attach(3);
            }

            $newGoogleAccount->user()->associate($user);
            $newGoogleAccount->save();

            return $user;
        }
    }
}
