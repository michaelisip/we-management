<?php

namespace App\Mail;

use App\Client;
use App\Traits\TemplateAction;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Compose extends Mailable
{
    use Queueable, SerializesModels, TemplateAction;

    public $email;

    public function __construct($email)
    {
        $this->email = $email;

        $recipient = Client::findOrFail($email['to']);
        $this->email['body'] = $this->variableSwap($recipient, $email);
    }

    public function build()
    {
        return $this->subject($this->email['subject'])
                    ->markdown('mails.compose');
    }
}
