<?php

namespace App\Mail\Discovery;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Success extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct()
    {
        // 
    }

    public function build()
    {
        return $this->subject('Discovery Success')
                    ->markdown('mails.discovery-trigger.success');
    }
}
