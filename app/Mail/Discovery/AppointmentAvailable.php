<?php

namespace App\Mail\Discovery;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AppointmentAvailable extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct()
    {
        // 
    }

    public function build()
    {
        return $this->subject('Appointment Available')
                    ->markdown('mails.discovery-trigger.appointment.available');
    }
}
