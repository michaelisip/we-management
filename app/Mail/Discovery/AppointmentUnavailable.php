<?php

namespace App\Mail\Discovery;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AppointmentUnavailable extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct()
    {
        // 
    }

    public function build()
    {
        return $this->subject('Appointment Unavailable')
                    ->markdown('mails.discovery-trigger.appointment.unavailable');
    }

}
