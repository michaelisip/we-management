<?php

namespace App\Traits;

use App\Client;
use App\Template as TemplateModel;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

trait TemplateAction
{
    public function fill(TemplateModel $template)
    {
        return response()->json(['template' => $template]);
    }

    public function variableSwap(Client $client, $mail)
    {
        try {
            $body = $mail['body'];

            // refactor to array and search to replace
            $body = str_replace('{{To: First Name}}', $client->first_name, $body);
            $body = str_replace('{{To: Email}}', $client->email, $body);
            $body = str_replace('{{From: First Name}}', config('mail.from.name'), $body);
            $body = str_replace('{{From: Email}}', config('mail.from.address'), $body);

            return $body;

        } catch (Throwable $th) {

            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }
    }

    public function uploadImage(Request $request)
    {
        if($request->hasFile('upload')) {
            try {
                if (config('filesystems.disks.s3.key')) {
                    $path = Storage::disk('s3')->put('wemanagement/image', $request->file('upload'), 'public');
                    $path = config('filesystems.disks.s3.url') . '/' . $path;
                } else {
                    $path = Storage::disk('public')->put('', $request->file('upload'));
                    $path = config('app.url') . '/storage/' . $path;
                }
            } catch (Throwable $th) {
                Log::error($th->getMessage());
            }

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $msg = 'Image uploaded successfully';
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$path', '$msg')</script>";

            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }
    }
}
