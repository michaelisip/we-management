<?php

namespace App\Traits;

use Throwable;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

trait Media
{
    /** For both files and image  */
    public function upload($media, $type = 'image')
    {
        try {
            if (config('filesystems.disks.s3.key')) {
                $path = Storage::disk('s3')->put('wemanagement/' . Str::plural($type), $media, 'public');
                $path = config('filesystems.disks.s3.url') . '/' . $path;
            } else {
                $path = Storage::disk('public')->put('', $media);
                $path = config('app.url') . '/storage/' . $path;
            }
        } catch (Throwable $th) {
            Log::error($th->getMessage());

            return response()->json([
                'success' => false,
                $type => null,
            ]);
        }

        return $path;
    }
}
