<?php

namespace App\Traits;

use App\User;
use App\Client;
use App\Template;
use App\Appointment;

use Illuminate\Http\Request;
use Spatie\Searchable\Search;
use Illuminate\Database\Eloquent\Model;

trait Searchable
{
    // search function on tables
    public function search($request, $model)
    {
        if ($model instanceof Client) {
            if ($request->query('search')) {
                $model = $model->where('first_name', 'LIKE', "%{$request->query('search')}%")
                                ->orWhere('last_name', 'LIKE', "%{$request->query('search')}%")
                                ->orWhere('email', 'LIKE', "%{$request->query('search')}%");
            }
        }

        if ($model instanceof Appointment) {
            $model = $model->where('type', 'LIKE', "%{$request->query('search')}%")
                                ->orWhere('description', 'LIKE', "%{$request->query('search')}%")
                                ->orWhere('notes', 'LIKE', "%{$request->query('search')}%")
                                ->orWhere('status', 'LIKE', "%{$request->query('search')}%")
                                ->orWhereHas('client', function($query) use($request) {
                                    $query->where('first_name', 'LIKE', "%{$request->query('search')}%")
                                            ->orWhere('last_name', 'LIKE', "%{$request->query('search')}%")
                                            ->orWhere('email', 'LIKE', "%{$request->query('search')}%");
                                });
        }

        if ($model instanceof Template) {
            $model = $model->where('name', 'LIKE', "%{$request->query('search')}%")
                                ->orWhere('keyword', 'LIKE', "%{$request->query('search')}%")
                                ->orWhere('subject', 'LIKE', "%{$request->query('search')}%")
                                ->orWhere('body', 'LIKE', "%{$request->query('search')}%");
        }

        if ($model instanceof User) {
            $model = $model->where('first_name', 'LIKE', "%{$request->query('search')}%")
                                ->orWhere('last_name', 'LIKE', "%{$request->query('search')}%")
                                ->orWhere('email', 'LIKE', "%{$request->query('search')}%");
        }

        if ($request->query('sort')) {
            $isDesc = $request->query('desc') ? 'DESC' : 'ASC';
            $model = $model->orderBy($request->query('sort'), $isDesc);
        }

        return $model;
    }

    // general search function suggestions
    public function suggestions(Request $request)
    {
        $suggestions = (new Search())
            ->registerModel(Client::class, 'first_name', 'last_name')
            ->registerModel(Appointment::class, 'description')
            ->registerModel(Template::class, 'name')
            ->search($request->keyword);

        return $suggestions;
    }
}
