<?php

namespace App\Traits;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;

trait ActiveCampaign
{
    private $total = null;

    private $limit = 20;

    private $offset = 0;

    private $activeCampaignContacts = [];

    // Note: API has a rate limit of 5 requests per second per account.
    public function getContacts()
    {
        try {
            $this->listAllContacts();

        } catch (Throwable $th) {

            Log::error($th->getMessage());
            return null;
        }

        return $this->activeCampaignContacts;
    }

    // https://developers.activecampaign.com/reference#pagination
    public function listAllContacts()
    {
        do {
            $response = Http::withHeaders([
                'Token' => config('services.activecampaign.api_key')
            ])->get(config('services.activecampaign.base_url') . '/accounts');

            if ($response->failed()) {
                $response->throw();
            }

            $results = $response->json();

            array_push($this->activeCampaignContacts, $results->contacts);

            if (is_null($this->total)) {
                $this->total = $results->meta->total;
            }

        } while ($this->offset <= $this->total);
    }
}
