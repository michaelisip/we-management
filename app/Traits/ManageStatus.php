<?php

namespace App\Traits;

use App\Client as ClientModel;
use App\Appointment as AppointmentModel;

use Throwable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait ManageStatus
{
    public function archive(ClientModel $client)
    {
        DB::beginTransaction();

        try {
            $client->status = 'archived';
            $client->archived_at = now();
            $client->save();

        } catch (Throwable $th) {

            DB::rollBack();
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        DB::commit();
        return redirect()->route('clients.index');
    }

    public function inactive(ClientModel $client)
    {
        DB::beginTransaction();

        try {
            $client->status = 'inactive';
            $client->save();

        } catch (Throwable $th) {

            DB::rollBack();
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        DB::commit();
        return redirect()->route('clients.index');
    }

    public function done(AppointmentModel $appointment)
    {
        DB::beginTransaction();

        try {
            $appointment->status = 'done';
            $appointment->save();

        } catch (Throwable $th) {

            DB::rollBack();
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        DB::commit();
        return redirect()->route('appointments.index');
    }

    public function cancel(AppointmentModel $appointment)
    {
        DB::beginTransaction();

        try {
            $appointment->status = 'cancelled';
            $appointment->cancelled_at = now();
            $appointment->save();

        } catch (Throwable $th) {

            DB::rollBack();
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        DB::commit();
        return redirect()->route('appointments.index');
    }
}
