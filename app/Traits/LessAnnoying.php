<?php

namespace App\Traits\Manage;

use Throwable;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

trait LessAnnoying
{
    private $endpoint = "https://api.lessannoyingcrm.com";

    public function getContact($contactId)
    {
        $parameters = [
            'ContactId' => $contactId
        ];

        try {
            $response = Http::get($this->endpoint, [
                'UserCode' => config('services.lessannoying.user_code'),
                'APIToken' => config('services.lessannoying.api_token'),
                'Function' => 'GetClient',
                'Parameters' => urlencode(json_encode($parameters))
            ]);

            $result = json_decode($response, true);

            if ($result['Success'] === true) {
                dd('success', $result);
            } else {
                dd('error', $result['Error']);
            }

        } catch (Throwable $th) {

            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        return redirect()->route('clients.index');
    }
}
