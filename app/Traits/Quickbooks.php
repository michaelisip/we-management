<?php

namespace App\Traits;

use Throwable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use QuickBooksOnline\API\DataService\DataService;

trait Quickbooks
{
    public function invoice()
    {
        try {
            $dataService = DataService::Configure([
                'auth_mode' => 'oauth2',
                'ClientID' => config('services.quickbooks.client_id'),
                'ClientSecret' => config('services.quickbooks.client_secret'),
                'RedirectURI' => route('invoices.quickbooks.callback'),
                'scope' => "com.intuit.quickbooks.accounting",
                'baseUrl' => config('app.env') === 'production' ? "Production" : "Development"
            ]);

            $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
            $authorizationCodeUrl = $OAuth2LoginHelper->getAuthorizationCodeURL();

            Session::put('OAuth2LoginHelper', $OAuth2LoginHelper);
            return redirect($authorizationCodeUrl);

        } catch (Throwable $th) {

            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }
    }

    public function invoiceCallback(Request $request)
    {
        try {
            $OAuth2LoginHelper = Session::get('OAuth2LoginHelper');

            $accessTokenObj = $OAuth2LoginHelper->exchangeAuthorizationCodeForToken($request['code'], $request['realmId']); 

            Session::put('realmId', $request['realmId']);
            Session::put('accessToken', $accessTokenObj->getAccessToken());
            Session::put('refreshToken', $accessTokenObj->getRefreshToken());
            Session::put('QBSessionExpireAt', Carbon::now()->addHour());
            Session::forget('OAuth2LoginHelper');

        } catch (Throwable $th) {

            Log::error($th->getMessage());
        }

        return redirect()->route('invoices.index');
    }

    public function dataService()
    {
        $dataService = DataService::Configure([
            'auth_mode' => 'oauth2',
            'ClientID' => config('services.quickbooks.client_id'),
            'ClientSecret' => config('services.quickbooks.client_secret'),
            'accessTokenKey' => Session::get('accessToken'),
            'refreshTokenKey' => Session::get('refreshToken'),
            'QBORealmID' => Session::get('realmId'),
            'baseUrl' => config('app.env') === 'production' ? 'Production' : "Development"
        ]);

        $dataService->throwExceptionOnError(true);

        return $dataService;
    }
}
