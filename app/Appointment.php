<?php

namespace App;

use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointment extends Model implements Searchable
{
    use SoftDeletes;

    protected $fillable = [
        'client_id', 'type', 'date_time', 'description', 'notes', 'status'
    ];

    protected $dates = [
        'date_time',
        'cancelled_at',
        'updated_at',
        'created_at',
    ];

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function files()
    {
        return $this->morphMany('App\File', 'fileable');
    }

    public static function types() : array
    {
        return [
            'consultation-call',
            'others'
        ];
    }

    public static function statuses() : array
    {
        return [
            'pending',
            'done',
            'cancelled'
        ];
    }

    public function getSearchResult() : SearchResult
    {
        $url = route('appointments.show', $this->id);
        $title = $this->client->fullname . "'s appointment";

        return new SearchResult(
            $this,
            $title,
            $url,
        );
    }

    public function getStyleClassesAttribute()
    {
        if ($this->status === 'pending') {
            return 'bg-green-100 text-green-800';
        } elseif ($this->status === 'cancelled') {
            return 'bg-orange-200 text-orange-800';
        } else {
            return 'bg-blue-200 text-blue-800';
        }
    }
}
