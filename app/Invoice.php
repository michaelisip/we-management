<?php

namespace App;

use Carbon\Carbon;
use App\Scopes\VoidedScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'qb_invoice_id', 'invoice_link', 'bill_email'
    ];

    protected static function booted()
    {
        static::addGlobalScope(new VoidedScope);
    }

    public function getLastUpdatedAttribute()
    {
    	return Carbon::parse("$this->updated_at")->format('F d, Y h:i A');
    }

    public function getCreatedAttribute()
    {
    	return Carbon::parse("$this->created_at")->format('F d, Y h:i A');
    }
}
