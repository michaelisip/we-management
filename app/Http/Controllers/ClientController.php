<?php

namespace App\Http\Controllers;

use App\File;
use App\Group;
use App\Image;
use App\Client;
use App\Address;
use App\Traits\Media;
use App\Traits\ManageStatus;
use App\Http\Requests\ClientStore;
use App\Http\Requests\ClientUpdate;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ClientController extends Controller
{
    use ManageStatus, Media;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $clients = $this->search($request, new Client);

        return view('clients.index')->withClients($clients->latest()->paginate());
    }

    public function create()
    {
        return view('clients.create');
    }

    public function store(ClientStore $request)
    {
        DB::beginTransaction();

        try {
            $client = Client::create($request->validated());

            $client->addresses()->save(new Address([
                'country' => $request->country,
                'street' => $request->street,
                'state' => $request->state,
                'city' => $request->city,
                'zip' => $request->zip,
            ]));

            if ($request->has('image')) {
                $path = $this->upload($request->file('image'), 'image');

                $client->image()->save(new Image([
                    'filename' => $request->file('image')->getClientOriginalName(),
                    'url' => $path,
                ]));
            }

            if ($request->has('group')) {
                foreach ($request->group as $group) {
                    $group = Group::firstOrCreate(['id' => $group], ['name' => $group]);
                    $client->groups()->attach($group->id);
                }
            }

            if ($request->has('file')) {
                foreach ($request->file as $file) {
                    $path = $this->upload($file, 'file');

                    $client->files()->save(new File([
                        'filename' => $file->getClientOriginalName(),
                        'url' => $path,
                    ]));
                }
            }

        } catch (Throwable $th) {

            DB::rollBack();
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        DB::commit();
        return redirect()->route('clients.index');
    }

    public function show(Client $client)
    {
        return view('clients.show')->withClient($client);
    }

    public function edit(Client $client)
    {
        return view('clients.edit')->withClient($client);
    }

    public function update(ClientUpdate $request, Client $client)
    {
        DB::beginTransaction();

        try {
            $client->update($request->validated());

            $client->addresses()->update([
                'country' => $request->country,
                'street' => $request->street,
                'state' => $request->state,
                'city' => $request->city,
                'zip' => $request->zip,
            ]);

            if ($request->has('image')) {
                $path = $this->upload($request->file('image'), 'image');

                $client->image()->save(new Image([
                    'filename' => $request->file('image')->getClientOriginalName(),
                    'url' => $path,
                ]));
            }

            if ($request->has('group')) {
                $client->groups()->detach();

                foreach ($request->group as $group) {
                    $group = Group::firstOrCreate(['id' => $group], ['name' => $group]);
                    $client->groups()->attach($group->id);
                }
            }

            if ($request->has('file')) {
                foreach ($request->file as $file) {
                    if ($file !== null) {
                        $path = $this->upload($file, 'file');
    
                        $client->files()->save(new File([
                            'filename' => $file->getClientOriginalName(),
                            'url' => $path,
                        ]));
                    }
                }
            }

        } catch (Throwable $th) {

            DB::rollBack();
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        DB::commit();
        return redirect()->route('clients.index');
    }

    public function destroy(Client $client)
    {
        DB::beginTransaction();

        try {
            $client->delete();

        } catch (Throwable $th) {

            DB::rollBack();
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        DB::commit();
        return redirect()->route('clients.index');
    }
}
