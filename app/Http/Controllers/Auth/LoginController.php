<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Services\Authentication\Google as GoogleService;

use Throwable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function google()
    {
        try {
            return Socialite::driver('google')->redirect();
        } catch (Throwable $th) {
            Log::error($th->getMessage());
        }

        return back()->withErrors(['Can\'t contact google at the moment.']);
    }

    public function googleCallback(GoogleService $service)
    {
        try {
            $socialUser = Socialite::driver('google')->user();
        } catch (Throwable $th) {
            return redirect()->route('login')->withErrors(['Can\'t fetch google information at the moment']);
        }

        DB::beginTransaction();

        try {
            $user = $service->createOrGetUser($socialUser);

            Auth::login($user);
            DB::commit();

        } catch (Throwable $th) {

            DB::rollBack();
            Log::error($th->getMessage());
            return redirect()->route('login')->withErrors(['Can\'t login using google.']);
        }

        return redirect()->route('dashboard');
    }
}
