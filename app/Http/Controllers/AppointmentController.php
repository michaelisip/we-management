<?php

namespace App\Http\Controllers;

use App\Client;
use App\Appointment;
use App\Traits\ManageStatus;
use App\Http\Requests\AppointmentStore;
use App\Http\Requests\AppointmentUpdate;

use Throwable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AppointmentController extends Controller
{
    use ManageStatus;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $appointments = $this->search($request, new Appointment);

        $futureAppointments = $appointments->where('date_time', '>', Carbon::now())
            ->orderBy('date_time', 'desc')
            ->get();

        $pastAppointments = $appointments->where('date_time', '<', Carbon::now())
            ->orderBy('date_time', 'asc')
            ->get();

        $appointments = $futureAppointments->merge($pastAppointments);

        return view('appointments.index')->withAppointments($appointments->collectionPaginate());
    }

    public function create()
    {
        return view('appointments.create')->withClients(Client::all());
    }

    public function store(AppointmentStore $request)
    {
        DB::beginTransaction();

        try {
            Appointment::create($request->validated());

        } catch (Throwable $th) {

            Log::error($th->getMessage());
            DB::rollBack();
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        DB::commit();
        return redirect()->route('appointments.index')->withSuccess('Appointment created successfully.');
    }

    public function show(Appointment $appointment)
    {
        return view('appointments.show')->withAppointment($appointment);
    }

    public function edit(Appointment $appointment)
    {
        return view('appointments.edit')->with([
            'clients' => Client::all(),
            'appointment' => $appointment
        ]);
    }

    public function update(AppointmentUpdate $request, Appointment $appointment)
    {
        DB::beginTransaction();

        try {
            $appointment->was_updated = 1;
            $appointment->update($request->validated());

        } catch (Throwable $th) {

            DB::rollBack();
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        DB::commit();
        return redirect()->route('appointments.index')->withSuccess('Appointment updated successfully.');
    }

    public function destroy(Appointment $appointment)
    {
        DB::beginTransaction();

        try {
            $appointment->delete();
        } catch (Throwable $th) {

            DB::rollBack();
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        DB::commit();
        return redirect()->route('appointments.index')->withSuccess('Appointment deleted successfully.');
    }
}
