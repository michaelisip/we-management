<?php

namespace App\Http\Controllers;

use App\Tag;
use App\File;
use App\Template;
use App\Traits\Media;
use App\Traits\TemplateAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\TemplateStore;
use App\Http\Requests\TemplateUpdate;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TemplateController extends Controller
{
    use TemplateAction, Media;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $templates = $this->search($request, new Template);

        return view('templates.index')->withTemplates($templates->with('tags')->latest()->paginate());
    }

    public function create()
    {
        return view('templates.create');
    }

    public function store(TemplateStore $request)
    {
        DB::beginTransaction();

        try {
            $templateTagIds = [];
            foreach ((array) $request->tags as $tag) {
                $exists = Tag::find($tag);
                if (! $exists) {
                    $tag = Tag::insertGetId([
                        'name' => $tag,
                        'updated_at' => now(),
                        'created_at' => now(),
                    ]);
                }
                array_push($templateTagIds, $tag);
            }

            $request->request->add(['tags' => $templateTagIds]);

            $template = Template::create($request->validated());
            $template->tags()->attach($request->tags);

            if ($request->has('attachments')) {
                foreach ($request->attachments as $attachment) {
                    $path = $this->upload($attachment, 'attachment');

                    $template->attachments()->save(new File([
                        'filename' => $attachment->getClientOriginalName(),
                        'url' => $path,
                    ]));
                }
            }

        } catch (Throwable $th) {

            DB::rollBack();
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        DB::commit();
        return redirect()->route('templates.index');
    }

    public function show(Template $template)
    {
        //
    }

    public function edit(Template $template)
    {
        return view('templates.edit')->withTemplate($template);
    }

    public function update(TemplateUpdate $request, Template $template)
    {
        DB::beginTransaction();

        try {
            $templateTagIds = [];

            foreach ((array) $request->tags as $tag) {
                $exists = Tag::find($tag);
                if (! $exists) {
                    $tag = Tag::insertGetId([
                        'name' => $tag,
                        'updated_at' => now(),
                        'created_at' => now(),
                    ]);
                }
                array_push($templateTagIds, $tag);
            }

            $request->request->add(['tags' => $templateTagIds]);

            $template->update($request->validated());
            $template->tags()->detach();
            $template->tags()->attach($request->tags);

            if ($request->has('attachments')) {
                foreach ($request->attachments as $attachment) {
                    $path = $this->upload($attachment, 'attachment');

                    $template->attachments()->save(new File([
                        'filename' => $attachment->getClientOriginalName(),
                        'url' => $path,
                    ]));
                }
            }

        } catch (Throwable $th) {

            DB::rollBack();
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        DB::commit();
        return redirect()->route('templates.index');
    }

    public function destroy(Template $template)
    {
        DB::beginTransaction();

        try {
            $template->delete();
        } catch (Throwable $th) {

            DB::rollBack();
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        DB::commit();
        return redirect()->route('templates.index');
    }
}
