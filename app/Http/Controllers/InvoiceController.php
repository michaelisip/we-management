<?php

namespace App\Http\Controllers;

use App\Invoice as InvoiceModel;
use App\Traits\Quickbooks;
use App\Http\Requests\InvoiceStore;
use App\Http\Requests\InvoiceUpdate;

use Throwable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use QuickBooksOnline\API\Facades\Invoice;

class InvoiceController extends Controller
{
    use Quickbooks;

    public function __construct() 
    {
        $this->middleware('quickbooks')->except(['invoice', 'invoiceCallback']);
    }

    public function index()
    {
        return view('invoices.index')->withInvoices(InvoiceModel::paginate());
    }

    public function create()
    {
        return view('invoices.create');
    }

    public function store(InvoiceStore $request)
    {
        DB::beginTransaction();

        try {
            $newInvoice = Invoice::create([
                "Line" => [
                    [
                        "Amount" => number_format($request->amount, 2),
                        "DetailType" => "SalesItemLineDetail",
                        "SalesItemLineDetail" => [
                            "ItemRef" => [
                                "name" => $request->service_name,
                                "value" => 1
                            ]
                        ]
                    ]
                ],
                "CustomerRef"=> [
                    "value"=> 1
                ],
                "BillEmail" => [
                    "Address" => $request->bill_email
                ],
                "BillEmailCc" => [
                    "Address" => "a@intuit.com"
                ],
                "BillEmailBcc" => [
                    "Address" => "v@intuit.com"
                ]
            ]);
            $response = $this->dataService()->Add($newInvoice);

            // Save Invoice details
            $invoice = new InvoiceModel;
            $invoice->qb_invoice_id = $response->Id;
            $invoice->invoice_link = $response->InvoiceLink;
            $invoice->bill_email = $response->BillEmail->Address;
            $invoice->service_name = $request->service_name;
            $invoice->save();

        } catch (Throwable $th) {

            DB::rollBack();
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        DB::commit();
        return redirect()->route('invoices.index');
    }

    public function show()
    {
        try {
            $invoice = $this->dataService()->FindbyId('invoice', 196);

        } catch (Throwable $th) {
    
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        return view('invoices.show')->withInvoice($invoice);
    }

    public function edit($id)
    {   
        try {
            $invoice = $this->dataService()->FindbyId('invoice', $id);
            $invoiceLocalDetail = InvoiceModel::where('qb_invoice_id', $id)->first();

        } catch (Throwable $th) {
    
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        return view('invoices.edit')->with(['invoice' => $invoice, 'localDetail' => $invoiceLocalDetail]);
    }

    public function update(InvoiceUpdate $request, $id)
    {
        try {
            $invoiceLocalDetail = InvoiceModel::find($id);
            $invoice = $this->dataService()->FindbyId('invoice', $invoiceLocalDetail->qb_invoice_id);

            $updatedInvoice = Invoice::update($invoice, [
                "Line" => [
                    [
                        "Amount" => number_format($request->amount, 2),
                        "DetailType" => "SalesItemLineDetail",
                        "SalesItemLineDetail" => [
                            "ItemRef" => [
                                "name" => $invoiceLocalDetail->service_name,
                                "value" => 1
                            ]
                        ]
                    ]
                ],
                "CustomerRef"=> [
                    "value"=> 2
                ],
                "BillEmail" => [
                    "Address" => $request->bill_email
                ],
                "BillEmailCc" => [
                    "Address" => "abc@intuit.com"
                ],
                "BillEmailBcc" => [
                    "Address" => "vxyz@intuit.com"
                ]
            ]);
    
            $response = $this->dataService()->Update($updatedInvoice);

            $invoice = $invoiceLocalDetail;
            $invoice->qb_invoice_id = $response->Id;
            $invoice->invoice_link = $response->InvoiceLink;
            $invoice->bill_email = $response->BillEmail->Address;
            $invoice->service_name = $request->service_name;
            $invoice->save();
    
        } catch (Throwable $th) {
    
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        return redirect()->route('invoices.index');
    }

    public function destroy($id)
    {
        try {
            $localDetail = InvoiceModel::find($id);
            $invoice = $this->dataService()->FindbyId('invoice', $localDetail->qb_invoice_id);
            $response = $this->dataService()->Delete($invoice);

            InvoiceModel::destroy($id);
    
        } catch (Throwable $th) {
    
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        return back();
    }

    public function void($id){
        try {
            $localDetail = InvoiceModel::find($id);
            $invoice = $this->dataService()->FindbyId('invoice', $localDetail->qb_invoice_id);
            $response = $this->dataService()->Void($invoice);

            $localDetail->voided_at = Carbon::now();
            $localDetail->save();
    
        } catch (Throwable $th) {
    
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        return back();
    }

    public function sendEmail($id){
        try{
            $invoice = $this->dataService()->FindById("invoice", $id);
            $this->dataService()->SendEmail($invoice, $invoice->BillEmail->Address);
        }catch(Throwable $th){
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        return redirect()->back();
    }

}
