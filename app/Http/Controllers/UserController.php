<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\Image;
use App\Traits\Media;
use App\Http\Requests\UserStore;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    use Media;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $users = $this->search($request, new User);

        return view('users.index')->withUsers($users->latest()->paginate());
    }

    public function create()
    {
        return view('users.create')->withRoles(Role::all());
    }

    public function store(UserStore $request)
    {
        DB::beginTransaction();

        try {
            $user = User::create($request->validated());

            foreach ($request->roles as $role) {
                $user->roles()->attach($role);
            }

            if ($request->has('image')) {
                $path = $this->upload($request->file('image'), 'image');

                $user->image()->save(new Image([
                    'filename' => $request->file('image')->getClientOriginalName(),
                    'url' => $path,
                ]));
            }

        } catch (Throwable $th) {

            DB::rollBack();
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        DB::commit();
        return redirect()->route('users.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
