<?php

namespace App\Http\Controllers;

use App\Client;
use App\Template;
use App\Mail\Compose;
use App\Http\Controllers\Controller;
use App\Http\Requests\MailSend;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class MailController extends Controller
{
    public function compose()
    {
        return view('compose')->with([
            'clients' => Client::all(),
            'templates' => Template::all(),
        ]);
    }

    // Note: system only allows manual email sending on one recipient atm
    public function send(MailSend $request)
    {        
        try {
            $recipient = Client::findOrFail($request->validated()['to']);

            Mail::to($recipient->email)
                ->cc($request->validated()['cc'] ?? [])
                ->bcc($request->validated()['bcc'] ?? [])
                ->send(new Compose($request->validated()));

        } catch (Throwable $th) {
            Log::error($th->getMessage());
            return back()->withErrors(['Something went wrong. Please contact support.']);
        }

        return back();
    }
}
