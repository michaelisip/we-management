<?php

namespace App\Http\Controllers;

use App\Client;
use App\Appointment;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $activeClientsCount = Client::whereStatus('active')->count();
        $pendingAppointmentsCount = Appointment::whereStatus('pending')->count();

        return view('dashboard')->with([
            'activeClients' => $activeClientsCount,
            'pendingAppointments' => $pendingAppointmentsCount,
            'recentActivities' => collect()
        ]);
    }
}
