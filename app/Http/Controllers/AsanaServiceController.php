<?php

namespace App\Http\Controllers;

use Asana\Client;
use Illuminate\Http\Request;

class AsanaServiceController extends Controller
{
    public function __construct(){
    	$this->asana_client = Client::accessToken(config('services.asana.acces_code'));
    }

    public function __invoke(){
    	// Get the list of Asana's projects
		$projects = $this->asana_client->get('/projects', []);

		// Get the list of tasks of a certain project
		$tasks = $this->asana_client->get('/projects/' . $projects[1]->gid . '/tasks', []);
		dd($tasks);
    }
}
