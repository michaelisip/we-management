<?php

namespace App\Http\Controllers\API;

use App\Client;
use App\Appointment;
use App\Mail\Discovery\Success;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\DiscoveryTrigger;
use App\Mail\Discovery\AppointmentAvailable;
use App\Mail\Discovery\AppointmentUnavailable;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class DiscoveryController extends Controller
{
    private $request;

    private $client;

    public function trigger(Request $request)
    {
        // Note: added for discovery form
        if ($request->appointment === null) {
            $request->request->add(['appointment' => false]);
            $request->request->remove('appointment_date_time');
        } else {
            $request->request->add(['appointment' => true]);
        }

        // Note: this validator is only for testing, use DiscoveryTrigger for api
        $request->validate([
            'type' => 'required|in:' . implode(',', Client::types()),
            'first_name' => 'required|string',
            'last_name' => 'nullable|string',
            'email' => 'required|email',
            'status' => 'nullable"in:' . implode(',', Client::statuses()),
            'notes' => 'nullable|string',
            'image' => 'nullable|image',
            'file.*' => 'nullable|file',
            'appointment' => 'nullable|boolean',
            'appointment_type' => 'required_if:appointment,true|in:' . implode(',', Appointment::types()),
            'appointment_date_time' => 'required_if:appointment,true|date',
            'appointment_description' => 'nullable|string',
            'appointment_notes' => 'nullable|string',
            'appointment_status' => 'nullable|in:' . implode(',', Appointment::statuses()),
        ]);

        DB::beginTransaction();

        try {
            $this->request = $request;

            $this->storeClientIfNotExists();
            $this->registerAppointmentIfSelectedAndAvailable();

            Mail::to($this->client->email)->send(new Success);
            // ** send automatic mail 3 days before appointment

        } catch (Throwable $th) {

            DB::rollback();
            Log::error($th->getMessage());
            return response([
                'success' => false,
                'error' => $th->getMessage()
            ], 400);
        }

        DB::commit();
        return response([
            'success' => true,
            'error' => null,
        ]);
    }

    public function storeClientIfNotExists()
    {
        Log::info("Storing client from discovery trigger: " .  now());

        $clientQuey = Client::whereEmail($this->request->email);

        if ($clientQuey->exists()) {
            $this->client = $clientQuey->first();
        } else {
            $newClient = new Client;
            $newClient->type = $this->request->type;
            $newClient->first_name = $this->request->first_name;
            $newClient->last_name = $this->request->last_name;
            $newClient->middle_name = $this->request->middle_name;
            $newClient->suffix = $this->request->suffix;
            $newClient->company_name = $this->request->company_name;
            $newClient->job_title = $this->request->job_title;
            $newClient->website_link = $this->request->website_link;
            $newClient->email = $this->request->email;
            $newClient->birthday = $this->request->birthday;
            $newClient->status = $this->request->status ?? 'active';
            $newClient->background_info = $this->request->background_info;
            // $newClient->notes = $this->request->notes;
            $newClient->save();

            $this->client = $newClient;
        }

        return;
    }

    public function registerAppointmentIfSelectedAndAvailable()
    {
        Log::info("Registering appointment from discovery trigger: " . now());

        if (! $this->request->appointment) {
            return;
        }

        // TODO: logic for determining if appointment is unavailable
        if ($this->request->date_time === 'not available') {
            Mail::to($this->client->email)->send(new AppointmentUnavailable);
            return;
        }

        $newAppointment = new Appointment;
        $newAppointment->client_id = $this->client->id;
        $newAppointment->type = $this->request->appointment_type;
        $newAppointment->date_time = $this->request->appointment_date_time;
        $newAppointment->description = $this->request->appointment_description;
        $newAppointment->notes = $this->request->appointment_notes;
        $newAppointment->status = $this->request->appointment_status ?? 'pending';
        $newAppointment->save();

        Mail::to($this->client->email)->send(new AppointmentAvailable);
        return;
    }
}
