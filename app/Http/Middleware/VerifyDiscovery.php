<?php

namespace App\Http\Middleware;

use Closure;

class VerifyDiscovery
{
    private $discoveryClientUri = 'https://wemanagementservices.com/';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // TODO: Use laravel passport for security

        if ($request->url() !== $this->discoveryClientUri) {
            return response()->json([], 403);
        }

        return $next($request);
    }
}
