<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class QuickBooks
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::has('refreshToken')){
            if(Carbon::now()->lte(Session::get('QBSessionExpireAt'))){
                return $next($request);
            }

            return redirect()->route('invoices.quickbooks');
        }
        
        return redirect()->route('invoices.quickbooks');
    }
}
