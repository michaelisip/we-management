<?php

namespace App\Http\Requests;

use App\Client;
use Illuminate\Foundation\Http\FormRequest;

class ClientUpdate extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'type' => 'required|in:' . implode(',', Client::types()),
            'first_name' => 'required|string',
            'last_name' => 'nullable|string',
            'middle_name' => 'nullable|string',
            'suffix' => 'nullable|string',
            'email' => 'required|email|unique:clients,email, ' . $this->client->id . ',id',
            'birthday' => 'nullable|date',
            'status' => 'required|in:' . implode(',', Client::statuses()),
            'company_name' => 'nullable|string',
            'job_title' => 'nullable|string',
            'website_link' => 'nullable|url',
            'timezone' => 'nullable|string',
            'background_info' => 'nullable|string',
            'archived_at' => 'nullable|required_if:status,archived|date',
            'notes' => 'nullable|string',
            'image' => 'nullable|mimes:png,jpg',
            'groups.*' => 'nullable|array',
            'file.*' => 'nullable|mimetypes:application/pdf|max:10000',
        ];
    }

    public function messages()
    {
        return [
            'image.size' => 'Selected image exceeded the maximum size of 1024mb.',
            'file.*.size' => 'One of the selected files exceeded the maximum size of 1024mb.',
        ];
    }
}
