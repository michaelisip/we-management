<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MailSend extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'to' => 'required|exists:clients,id',
            'subject' => 'required|string',
            'body' => 'required|string',
            'cc' => 'nullable|array',
            'cc.*' => 'nullable|email',
            'bcc' => 'nullable|array',
            'bcc.*' => 'nullable|email',
            'attachments' => 'nullable|array',
            'attachments.*' => 'nullable|file',
        ];
    }

    public function messages()
    {
        return [
            'to.exists' => 'The email does not belong to any client.',
        ];
    }
}
