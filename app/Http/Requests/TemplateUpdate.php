<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TemplateUpdate extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'keyword' => 'nullable|string|regex:/^\S*$/u|unique:templates,keyword,' . $this->template->id . ',id',
            'body' => 'required|string',
            'subject' => 'nullable|string',
            'to.*' => 'nullable|email',
            'cc.*' => 'nullable|email',
            'bcc.*' => 'nullable|email',
            'attachments.*' => 'nullable|file',
            'tags.*' => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'to.email' => 'The recipient must be a valid email address.',
            'cc.email' => 'The recipient must be a valid email address.',
            'bcc.email' => 'The recipient must be a valid email address.',
            'attachments.file' => 'The attachments must be a valid file.',
        ];
    }
}
