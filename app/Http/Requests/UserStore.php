<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStore extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'nullable|string',
            'email' => 'required|unique:clients,email|email',
            'roles.*' => 'required|exists:roles,id',
            'password' => 'required|string|min:8|confirmed',
            'image' => 'nullable|image',
        ];
    }

    public function messages()
    {
        return [
            'image.size' => 'Selected image exceeded the maximum size of 1024mb.',
        ];
    }
}
