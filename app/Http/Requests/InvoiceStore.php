<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceStore extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'service_name' => 'required|string',
            'bill_email'   => 'required|string',
            'amount'       => 'required|numeric|min:0'
        ];
    }
}
