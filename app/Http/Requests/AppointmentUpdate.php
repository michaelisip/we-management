<?php

namespace App\Http\Requests;

use App\Appointment;
use Illuminate\Foundation\Http\FormRequest;

class AppointmentUpdate extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'client_id' => 'required|exists:clients,id',
            'type' => 'required|in:' . implode(',', Appointment::types()),
            'date_time' => 'required|date',
            'description' => 'nullable|string',
            'notes' => 'nullable|string',
            'status' => 'required|in:' . implode(',', Appointment::statuses())
        ];
    }

    public function messages()
    {
        return [
            'client_id.required' => 'The client field is required.',
            'client_id.exists' => 'The client selected does not exists.',
            'date_time.required' => 'The date/time field is required.',
            'date_time.date' => 'The date/time field must be in YYYY-mm-dd H:i:s format.'
        ];
    }
}
