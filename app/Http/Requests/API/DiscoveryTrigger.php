<?php

namespace App\Http\Requests\API;

use App\Client;
use App\Appointment;

use Illuminate\Foundation\Http\FormRequest;

class DiscoveryTrigger extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'type' => 'required|in:' . implode(',', Client::types()),
            'first_name' => 'required|string',
            'last_name' => 'nullable|string',
            'email' => 'required|email',
            'status' => 'nullable"in:' . implode(',', Client::statuses()),
            'notes' => 'nullable|string',
            'image' => 'nullable|image',
            'file.*' => 'nullable|file',
            'appointment' => 'required|boolean',
            'appointment_type' => 'required_if:appointment,true|in:' . implode(',', Appointment::types()),
            'appointment_date_time' => 'required_if:appointment,true|date',
            'appointment_description' => 'nullable|string',
            'appointment_notes' => 'nullable|string',
            'appointment_status' => 'nullable|in:' . implode(',', Appointment::statuses()),
        ];
    }
}
