<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SocialAuth extends Model
{
    use SoftDeletes;

    protected $table = 'social_authentications';

    protected $fillable = [
        'user_id',
        'provider_id',
        'provider',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
