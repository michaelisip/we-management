<?php

namespace App;

use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Template extends Model implements Searchable
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'keyword', 'body', 'subject', 'to', 'cc', 'bcc'
    ];

    protected $casts = [
        'to' => 'array',
        'cc' => 'array',
        'bcc' => 'array',
    ];

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function attachments()
    {
        return $this->morphMany('App\File', 'fileable');
    }

    public function getSearchResult() : SearchResult
    {
        $url = route('templates.show', $this->id);

        return new SearchResult(
            $this,
            $this->name,
            $url,
        );
    }
}
