<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Email extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'email', 'email_type'
    ];

    public function emailable()
    {
        return $this->morphTo();
    }
}
