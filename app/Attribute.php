<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $table = 'attributes';

    protected $fillable = [
        'values' => 'array'
    ];

    public function attributable()
    {
        return $this->morphTo();
    }
}
